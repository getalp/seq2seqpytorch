from __future__ import unicode_literals, print_function, division
import logging
import os
import sys

try:
	reload(sys)
	sys.setdefaultencoding('utf-8')
except:
	pass

from torchtext_lite import torchtext_lite as torchtext
from utils import data_util
from utils import attention_viz, decode_sentence
from utils import checkpoint_util
from utils import arg_util
from utils.attribute_dic import AttrDict

from models.model_builder import build_seq2seq, build_transformer


import torch

logger = logging.getLogger(__name__)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")




class Generator:


	def __init__(self, opts, model, test_dataset, experiment_dir, output_file=None):
		
		self.opts = opts
		self.model = model
		self.test_dataset = test_dataset
		self.experiment_dir = experiment_dir
		self.output_file = None
		if output_file:
			self.output_file = os.path.join(self.experiment_dir, output_file)
			self.writer = open(self.output_file, 'w', encoding='utf-8')
		self.counter = 0


	def sort_batch(self, src, src_len, src_raw):
		
		src_len, idx_map = src_len.sort(descending=True)
		src = src[idx_map]
		src_raw = [src_raw[i] for i in idx_map]
		_, idx_map = idx_map.sort(0)

		return src, src_len, src_raw, idx_map


	def generate_step(self, batch, visualize=False):

		src, src_len = batch.src
		src_raw = batch.src_raw
		src, src_len, src_raw, idx_map = self.sort_batch(src, src_len, src_raw)

		if self.opts.pointer_gen:
			pg_src, max_oovs, oovs = self.test_dataset.pg_batch(src, src_raw)
		else:
			pg_src, max_oovs, oovs = None, 0, None
		

		with torch.no_grad():
			model_output = self.model.predict(self.opts, src=src, src_lengths=src_len, pg_src=pg_src, max_oovs=max_oovs)
		log_probs = model_output['log_probs']
		attn_weights = model_output['attn_weights']

		decoded_sentences = decode_sentence.decode(log_probs, attn_weights, src_raw, self.test_dataset.target_lang, self.opts.UNK_rep, oovs, self.opts.beam_size, split_level=opts.split_level)
		decoded_sentences = [decoded_sentences[i] for i in idx_map]

		#src_raw and attn_weights are only needed for visualization (this has to be done outside  the loop)
		if visualize:
			src_raw = [src_raw[i] for i in idx_map]
			attn_weights = [attn_weights[i] for i in idx_map]
			src_len = [src_len[i] for i in idx_map]

		for seq_i, decoded_sentence in enumerate(decoded_sentences):

			self.counter+=1
			
			_decoded_sentence = torchtext.detokenizer(decoded_sentence).replace('<EOS>', '').strip()
			print(self.counter, ': ', _decoded_sentence.encode('utf-8'))
			if self.output_file: self.writer.write(_decoded_sentence + '\n')

			if visualize:

				save_path = os.path.join(self.experiment_dir, 'attention')
				if not os.path.exists(save_path): os.makedirs(save_path)
				file_name = os.path.join(save_path, str(self.counter))
				in_sent_len = src_len[seq_i]
				out_sent_len = len(decoded_sentence)
				attention_viz.show_attention(torchtext.tokenizer(src_raw[seq_i]), decoded_sentence, attn_weights[seq_i][:out_sent_len, :in_sent_len], file_name)


	def generate(self, fields, trans_file=None, interactive=False, visualize=False):

		if interactive:
			while True:
				src_line = input('Input sequence: ')
				src_line = src_line.strip()
				src_line = torchtext.tokenizer(src_line)[:self.opts.src_max_length]
				src_line = torchtext.detokenizer(src_line)
				example = [torchtext.Example([src_line, src_line], fields)]
				batch = torchtext.Batch(example, dict(fields), device)
				self.generate_step(batch, visualize)
		else:

			examples = []
			with open(trans_file, 'r', encoding='utf-8') as trans_input:
				for src_line in trans_input:
					src_line = src_line.strip()

					src_line = torchtext.tokenizer(src_line)[:self.opts.src_max_length]
					src_line = torchtext.detokenizer(src_line)

					if src_line != '':
						examples.append(torchtext.Example([src_line, src_line], fields))

						if len(examples) == self.opts.batch_size:
							batch = torchtext.Batch(examples, dict(fields), device)
							self.generate_step(batch, visualize)
							examples = []

			if len(examples) > 0:
				batch = torchtext.Batch(examples, dict(fields), device)
				self.generate_step(batch, visualize)
				examples = []

		if self.output_file: self.writer.close()


if __name__ == '__main__':

	opts, config = arg_util.generator_args()

	if opts.split_level: torchtext.split_level = opts.split_level

	test_dataset, fields =	data_util.load_test_data(opts.src_vocab, opts.trg_vocab, share_vocab=opts.share_vocab, split_level=opts.split_level)

	opts.PAD_id = test_dataset.target_lang.PAD_id
	opts.UNK_id = test_dataset.target_lang.UNK_id
	opts.SOS_id = test_dataset.target_lang.SOS_id
	opts.EOS_id = test_dataset.target_lang.EOS_id

	if opts.transformer == True:
		model = build_transformer(opts,
			test_dataset.source_lang.n_words, 
			test_dataset.target_lang.n_words, 
			opts.hidden_size, 
			opts.enc_n_layers, 
			opts.dec_n_layers, 
			opts.n_head, 
			opts.feedforward_size, 
			opts.dropout)
	else:
		model = build_seq2seq(test_dataset.source_lang.n_words, 
			test_dataset.target_lang.n_words,
			opts.embedding_dim,
			opts.hidden_size,
			opts.enc_n_layers,
			opts.dec_n_layers, 
			opts.bi_rnn,
			opts.dropout,
			opts.rnn_type,
			None,
			None, 
			opts.trg_max_length,
			opts.pointer_gen,
			opts.coverage, 
			opts.attention_type, 
			opts.UNK_id,
			opts.PAD_id,
			opts.SOS_id,
			opts.EOS_id,
			opts.beam_size, 
			opts.beam_length_norm,
			opts)


	checkpoint = checkpoint_util.load_checkpoint(opts, opts.checkpoint)
	model.load_state_dict(checkpoint['model_state_dict'])
	model.eval()

	if not opts.experiment_dir:
		opts.experiment_dir = './experiments/'
	experiment_dir = os.path.join(opts.experiment_dir, opts.experiment_name)

	generator = Generator(opts, model, test_dataset, experiment_dir, opts.output_file)
	generator.generate(fields, trans_file=opts.gen_file, interactive=opts.interactive, visualize=opts.visualize)

