import torch.nn as nn
import torch
from .search import greedy_search, beam_search

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")



class Seq2seq(nn.Module):

	def __init__(self, encoder, decoder, bridge):
		super(Seq2seq, self).__init__()
		self.encoder = encoder
		self.decoder = decoder
		self.bridge = bridge


	def forward(self, src, src_lengths=None, trg=None, pg_src=None, max_oovs=None):

		encoder_outputs, encoder_hidden = self.encoder(src=src, src_lengths=src_lengths)

		if self.bridge is not None:
			encoder_hidden = self.bridge(encoder_hidden)
		else:
			encoder_hidden = None

		log_probs, decoder_hidden, attn_weights, coverage, p_gens = \
			self.decoder(trg=trg, encoder_hidden=encoder_hidden, encoder_outputs=encoder_outputs, 
			src_lengths=src_lengths, pg_src=pg_src, max_oovs=max_oovs)

		output = {'log_probs': log_probs, 'attn_weights':attn_weights, 'coverage': coverage, 'p_gens':p_gens}
		
		return output

	def predict(self, opts, src, src_lengths=None, pg_src=None, max_oovs=None):

		encoder_outputs, encoder_hidden = self.encoder(src=src, src_lengths=src_lengths)

		if self.bridge is not None:
			encoder_hidden = self.bridge(encoder_hidden)
		else:
			encoder_hidden = None

		if opts.beam_size  > 1:

			log_probs, decoder_hidden, attn_weights, coverage, p_gens = \
				beam_search(self.decoder, opts.pointer_gen, opts.coverage, opts.trg_max_length, self.decoder.vocab_size, 
				opts.SOS_id, opts.EOS_id, opts.UNK_id, opts.PAD_id, encoder_hidden=encoder_hidden, encoder_outputs=encoder_outputs, 
				src_lengths=src_lengths, pg_src=pg_src, max_oovs=max_oovs,beam_size=opts.beam_size, beam_length_norm=opts.beam_length_norm, 
				transformer=False)
		else:

			log_probs, decoder_hidden, attn_weights, coverage, p_gens = \
				greedy_search(self.decoder, opts.pointer_gen, opts.coverage, opts.trg_max_length, self.decoder.vocab_size, 
				opts.SOS_id, opts.EOS_id, opts.UNK_id, opts.PAD_id, encoder_hidden=encoder_hidden, encoder_outputs=encoder_outputs, 
				src_lengths=src_lengths, pg_src=pg_src, max_oovs=max_oovs)
		
		output = {'log_probs': log_probs, 'attn_weights':attn_weights, 'coverage': coverage, 'p_gens':p_gens}
		
		return output


	def summary(self):

		output_string = ''
		output_string += '='*100 + '\n'
		output_string += 'Model summary:' + '\n'
		output_string += str(self.encoder) + '\n'
		output_string += str(self.bridge) + '\n'
		output_string += str(self.decoder) + '\n'


		total_enc_params = sum(p.numel() for p in self.encoder.parameters() if p.requires_grad)
		total_bri_params = sum(p.numel() for p in self.bridge.parameters() if p.requires_grad) if self.bridge is not None else 0
		total_dec_params = sum(p.numel() for p in self.decoder.parameters() if p.requires_grad)
		total_params = total_enc_params + total_bri_params + total_dec_params

		output_string += 'Total nb of trainable parameters: ' + str(total_params) + '\n'
		output_string += '='*100 + '\n'

		return output_string

