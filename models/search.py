from utils.helper import tile, make_std_mask
import torch
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")



def greedy_search(decoder, use_pointer_gen, use_coverage, max_length, vocab_size, 
	sos_id, eos_id, unk_id, pad_id, encoder_hidden=None, encoder_outputs=None, 
	src_lengths=None, pg_src=None, max_oovs=0, src_mask=None, trg_mask=None):

	src_size = encoder_outputs.size(1)
	batch_size  = encoder_outputs.size(0)

	if use_coverage:
		coverage = torch.zeros(batch_size, src_size).to(device) # coverage vector [Batch x SrcTimeSteps]
	else:
		coverage = None

	hidden = encoder_hidden

	outputs = torch.zeros(batch_size, max_length, vocab_size + max_oovs).to(device)
	attn_weights = torch.zeros(batch_size, max_length, src_size).to(device)
	coverages = torch.zeros(batch_size, max_length, src_size).to(device)
	p_gens = torch.zeros(batch_size, max_length).to(device)

	decoder_input = torch.LongTensor([sos_id] * batch_size).view(batch_size, 1).to(device) # we start by feeding the BOS token

	for di in range(max_length):
		step_outputs, hidden, step_attn, coverage, p_gen = \
			decoder.forward_step(trg=decoder_input, encoder_outputs=encoder_outputs, hidden=hidden, 
				src_lengths=src_lengths, coverage=coverage, pg_src=pg_src, max_oovs=max_oovs, src_mask=src_mask, trg_mask=trg_mask)
		decoder_input = step_outputs.topk(1)[1].squeeze(1).detach()
		decoder_input[decoder_input >= vocab_size] = unk_id

		outputs[:, di] = step_outputs.squeeze(1)
		attn_weights[:, di] = step_attn.squeeze(1)

		if coverage is not None:
			coverages[:, di] = coverage.squeeze(1)

			# Update the coverage attention.
			coverage = coverage + step_attn.squeeze(1)
		
		if p_gen is not None:
			p_gens[:, di] = p_gen.squeeze(1)


	return outputs, hidden, attn_weights, coverages, p_gens




def greedy_search_transformer(decoder, max_length, vocab_size, sos_id, eos_id, unk_id, pad_id, 
	encoder_outputs=None, src_mask=None, trg_mask=None):

	src_size = encoder_outputs.size(1)
	batch_size  = encoder_outputs.size(0)

	outputs = torch.zeros(batch_size, max_length, vocab_size).to(device)
	attn_weights = torch.zeros(batch_size, max_length, src_size).to(device)
	coverages = torch.zeros(batch_size, max_length, src_size).to(device)
	p_gens = torch.zeros(batch_size, max_length).to(device)

	decoder_input = torch.LongTensor([sos_id] * batch_size).view(batch_size, 1).to(device) # we start by feeding the BOS token

	for di in range(max_length):

		trg_mask = make_std_mask(decoder_input, pad_id)

		step_outputs, hidden, step_attn, coverage, p_gen = \
			decoder.forward_step(trg=decoder_input, encoder_outputs=encoder_outputs, src_mask=src_mask, trg_mask=trg_mask)
		
		step_outputs = step_outputs[:, di, :]
		step_attn = step_attn[:, di]
		if coverage is not None:
			coverage = coverage[:, di]

		step_token = step_outputs.topk(1)[1].detach()
		
		decoder_input = torch.cat([decoder_input, step_token], dim=1)
		decoder_input[decoder_input >= vocab_size] = unk_id

		outputs[:, di] = step_outputs
		attn_weights[:, di] = step_attn

		if coverage is not None:
			coverages[:, di] = coverage.squeeze(1)
			coverage = coverage + step_attn.squeeze(1)
		
		if p_gen is not None:
			p_gens[:, di] = p_gen.squeeze(1)
		
		#break the loop if all the generated tokens are eos
		all_finished = step_token.eq(eos_id).all()
		if all_finished:
			break

	return outputs, hidden, attn_weights, coverages, p_gens



def beam_search(decoder, use_pointer_gen, use_coverage, max_length, vocab_size, sos_id, eos_id, unk_id, pad_id, 
	encoder_hidden=None, encoder_outputs=None, src_lengths=None, pg_src=None, max_oovs=0, beam_size=1, beam_length_norm=0, 
	src_mask=None, trg_mask=None, transformer=False):

	src_size = encoder_outputs.size(1)
	batch_size  = encoder_outputs.size(0)
	
	if use_coverage:
		coverage = torch.zeros(batch_size, src_size).to(device) # coverage vector [Batch x SrcTimeSteps]
	else:
		coverage = None

	token_storage = torch.zeros([batch_size * beam_size, max_length], dtype=torch.long, device=device)
	log_prob_storage = torch.zeros([batch_size * beam_size, max_length], device=device)
	attention_storage = torch.zeros([batch_size * beam_size, max_length, src_size], device=device)
	coverage_storage = torch.zeros([batch_size * beam_size, max_length, src_size], device=device)
	pgen_storage = torch.zeros([batch_size * beam_size, max_length], device=device)
	seq_len = None

	batch_offset = torch.arange(beam_size*batch_size, dtype=torch.long, device=device)
	u_batch_offset = batch_offset.view(batch_size,-1)
	batch_offset = tile(batch_offset.contiguous(), beam_size, 0)
	batch_offset = batch_offset.view(batch_size,-1)
	
	beam_offset = torch.arange(beam_size*beam_size*batch_size, dtype=torch.long, device=device)
	beam_offset = beam_offset.view(batch_size,-1)


	hidden = encoder_hidden
	decoder_input = torch.LongTensor([sos_id] * batch_size*beam_size).view(batch_size*beam_size, 1).to(device)

	#inflate tensors############################################
	if not transformer:
		hidden = tile(hidden, beam_size, 1)
	
	if transformer:
		src_mask = tile(src_mask, beam_size, 0)

	if use_coverage:
		coverage = tile(coverage, beam_size, 0)

	encoder_outputs = tile(encoder_outputs.contiguous(), beam_size, 0)
	
	if src_lengths is not None:
		src_lengths = tile(src_lengths.contiguous(), beam_size, 0)

	if use_pointer_gen:
		pg_src = tile(pg_src.contiguous(), beam_size, 0)
	#inflate tensors############################################



	for di in range(0, max_length):

		if transformer:
			trg_mask = make_std_mask(decoder_input, pad_id)

		step_outputs, hidden, step_attn, coverage, p_gen = \
			decoder.forward_step(trg=decoder_input, encoder_outputs=encoder_outputs, hidden=hidden, 
				src_lengths=src_lengths, coverage=coverage, pg_src=pg_src, max_oovs=max_oovs, src_mask=src_mask, trg_mask=trg_mask)
		
		if transformer:
			step_outputs = step_outputs[:, -1, :]
			step_attn = step_attn[:, -1]
			if coverage is not None:
				coverage = coverage[:, -1]
		
		log_prob, candidates = torch.topk(step_outputs, beam_size)

		candidates = candidates.squeeze(1).view(-1)
		log_prob = log_prob.squeeze(1).view(-1)
		step_attn = step_attn.squeeze(1)


		# prevent finished beams for growing
		eos_indices = decoder_input.eq(eos_id).any(keepdim=True, dim=1)
		eos_indices = tile(eos_indices, beam_size)
		eos_indices = eos_indices.squeeze(1)
		log_prob[eos_indices] = 0
		candidates[eos_indices] = eos_id

		log_prob_storage_exp = tile(torch.sum(log_prob_storage, 1), beam_size, 0)
		log_prob_sum = log_prob_storage_exp + log_prob


		if beam_length_norm > 0:

			#compute the sequence lengths
			seq_len = log_prob_storage[:, :di+1].eq(0)
			seq_len = (seq_len != 1)
			seq_len = torch.sum(seq_len, 1)
			seq_len = seq_len.float()

			len_penalty = ((5 + seq_len)**beam_length_norm) / 6**beam_length_norm
			len_penalty = tile(len_penalty, beam_size, 0)
			log_prob_sum /= len_penalty.float()

		log_prob_sum = log_prob_sum.view(batch_size,-1)

		if di == 0:
			log_prob_sum = log_prob_sum[:, :beam_size]
		
		selected_log_prob, select_indices = log_prob_sum.topk(beam_size)



		si_batch = torch.gather(batch_offset, 1, select_indices)
		si_batch = si_batch.view(-1)
		
		token_storage = token_storage[si_batch]
		log_prob_storage = log_prob_storage[si_batch]
		attention_storage = attention_storage[si_batch]
		coverage_storage = coverage_storage[si_batch]
		pgen_storage = pgen_storage[si_batch]


		si_beam = torch.gather(beam_offset, 1, select_indices)				
		si_beam = si_beam.view(-1)

		candidates = candidates[si_beam]
		log_prob = log_prob[si_beam]
		
		step_attn = tile(step_attn, beam_size, 0)
		step_attn = step_attn[si_beam]

		if coverage is not None:
			coverage = tile(coverage, beam_size, 0)
			coverage = coverage[si_beam]
		
		if p_gen is not None:
			p_gen = tile(p_gen, beam_size, 0)
			p_gen = p_gen[si_beam]

		token_storage[:, di] = candidates
		log_prob_storage[:, di] = log_prob
		attention_storage[:, di, :] = step_attn

		if coverage is not None:
			coverage = coverage.squeeze(1)
			coverage_storage[:, di] = coverage
			coverage = coverage + step_attn

		if p_gen is not None:
			p_gen = p_gen.squeeze(1)
			pgen_storage[:, di] = p_gen
		
		if hidden is not None and not transformer:
			if isinstance(hidden, tuple):
				h, c = hidden
				h = h[:, si_batch, :]
				c = c[:, si_batch, :]
				hidden = (h, c)
			else:
				hidden = hidden[:, si_batch, :]
		

		if transformer:
			# decoder_input = token_storage[:, :di+1]
			decoder_input = torch.cat([decoder_input[:, 0].unsqueeze(1), token_storage[:, :di+1]], dim=1)
		else:
			decoder_input = token_storage[:, di].unsqueeze(1)
		
		decoder_input[decoder_input >= vocab_size] = unk_id

		all_finished = decoder_input.eq(eos_id).any(dim=1).all()
		if all_finished:
			break


	sum_log_prob_storage = torch.sum(log_prob_storage, 1)
	
	if beam_length_norm > 0:
		len_penalty = ((5 + seq_len)**beam_length_norm) / 6**beam_length_norm
		sum_log_prob_storage /= len_penalty.float()
	
	_, final_indices = sum_log_prob_storage.view(batch_size, -1).topk(1)

	si_batch = torch.gather(u_batch_offset, 1, final_indices)
	si_batch = si_batch.view(-1)
			
	outputs = token_storage[si_batch].squeeze(1)
	attn_weights = attention_storage[si_batch].squeeze(1)
	coverages = coverage_storage[si_batch].squeeze(1)
	p_gens = pgen_storage[si_batch].squeeze(1)

	return outputs, hidden, attn_weights, coverages, p_gens

