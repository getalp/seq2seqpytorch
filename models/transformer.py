import math

import torch.nn as nn
import torch
import torch.nn.functional as F

from .search import greedy_search_transformer, beam_search
from utils.helper import make_std_mask
from models import TransformerEncoder, TransformerEncoderLayer, TransformerDecoder, TransformerDecoderLayer

import numpy as np

'''
http://nlp.seas.harvard.edu/2018/04/03/attention.html
https://towardsdatascience.com/how-to-code-the-transformer-in-pytorch-24db27c8f9ec
https://github.com/OpenNMT/OpenNMT-py/blob/master/onmt/modules/multi_headed_attn.py

https://pytorch.org/docs/stable/_modules/torch/nn/modules/transformer.html
https://pytorch.org/docs/stable/_modules/torch/nn/modules/activation.html#MultiheadAttention
https://pytorch.org/docs/stable/nn.html?highlight=transformer#torch.nn.Transformer

http://jalammar.github.io/illustrated-transformer/
'''



class Transformer(nn.Module):

	def __init__(self, opts, src_vocab, trg_vocab, d_model=512, enc_num_layers=6, dec_num_layers=6, n_head=8, d_ff=2048, dropout=0.1):
		super(Transformer, self).__init__()

		self.opts = opts

		encoder_layer = TransformerEncoderLayer(d_model, n_head, d_ff, dropout)
		decoder_layer = TransformerDecoderLayer(d_model, n_head, d_ff, dropout)

		self.encoder = TransformerEncoder(src_vocab, encoder_layer, d_model, enc_num_layers, dropout)
		self.decoder = TransformerDecoder(trg_vocab, decoder_layer, d_model, dec_num_layers, dropout)

		self._reset_parameters()


	def forward(self, src, src_lengths=None, trg=None, **kwargs):

		src_mask = (src != self.opts.PAD_id).unsqueeze(-2)
		trg_mask = make_std_mask(trg, self.opts.PAD_id)

		encoder_outputs = self.encoder(src, mask=src_mask)
		log_probs = self.decoder(trg, encoder_outputs, src_mask=src_mask, trg_mask=trg_mask)
		output = {'log_probs': log_probs, 'attn_weights':None, 'coverage': None, 'p_gens':None}

		return output

	def predict(self, opts, src, src_lengths=None, **kwargs):

		src_mask = (src != opts.PAD_id).unsqueeze(-2)
		encoder_outputs = self.encoder(src, mask=src_mask)

		if opts.beam_size  > 1:

			log_probs, decoder_hidden, attn_weights, coverage, p_gens = \
				beam_search(self.decoder, False, False, opts.trg_max_length, self.decoder.vocab_size, 
				opts.SOS_id, opts.EOS_id, opts.UNK_id, opts.PAD_id, encoder_hidden=None, encoder_outputs=encoder_outputs, 
				src_lengths=src_lengths, beam_size=opts.beam_size, beam_length_norm=opts.beam_length_norm, src_mask=src_mask,
				transformer=True)
		else:

			log_probs, decoder_hidden, attn_weights, coverage, p_gens = \
				greedy_search_transformer(self.decoder, opts.trg_max_length, self.decoder.vocab_size, 
				opts.SOS_id, opts.EOS_id, opts.UNK_id, opts.PAD_id, encoder_outputs=encoder_outputs, src_mask=src_mask)
		
		output = {'log_probs': log_probs, 'attn_weights':attn_weights, 'coverage': None, 'p_gens':None}
		
		return output


	def summary(self):

		output_string = ''
		output_string += '='*100 + '\n'
		output_string += 'Model summary:' + '\n'
		output_string += str(self.encoder) + '\n'
		output_string += str(self.decoder) + '\n'


		total_enc_params = sum(p.numel() for p in self.encoder.parameters() if p.requires_grad)
		total_dec_params = sum(p.numel() for p in self.decoder.parameters() if p.requires_grad)
		total_params = total_enc_params + total_dec_params

		output_string += 'Total nb of trainable parameters: ' + str(total_params) + '\n'
		output_string += '='*100 + '\n'

		return output_string


	def _reset_parameters(self):
		'''Initiate parameters in the transformer model.'''

		for p in self.parameters():
			if p.dim() > 1:
				nn.init.xavier_uniform_(p)
