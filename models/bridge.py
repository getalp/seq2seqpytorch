import torch.nn as nn
import torch

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")



class Bridge(nn.Module):
	
	def __init__(self, hidden_size, n_directions, enc_n_layers, dec_n_layers, rnn_type, neural=False):
		super(Bridge, self).__init__()

		self.hidden_size = hidden_size
		self.n_directions = n_directions
		self.enc_n_layers = enc_n_layers
		self.dec_n_layers = dec_n_layers
		self.bidirectional = True if n_directions > 1 else False
		self.rnn_type = rnn_type
		self.neural = neural

		if self.neural:
			self.bridge_h = nn.Linear(hidden_size*n_directions*enc_n_layers, hidden_size*n_directions*dec_n_layers)
			if rnn_type == 'LSTM':
				self.bridge_c = nn.Linear(hidden_size*n_directions*enc_n_layers, hidden_size*n_directions*dec_n_layers)


	def forward(self, hidden):

		
		if self.neural:

			if self.enc_n_layers != self.dec_n_layers:

				if self.rnn_type == 'GRU':
					enc_n_layer, batch_size, hidden_size = hidden.size()
					hidden = hidden.view(batch_size, -1)
					hidden = self.bridge_h(hidden)
					hidden = hidden.view(self.dec_n_layers, batch_size, -1)

				else:
					h, c = hidden
					enc_n_layer, batch_size, hidden_size = h.size()
					
					h = h.view(batch_size, -1)
					c = c.view(batch_size, -1)
					
					h = self.bridge_h(h)
					c = self.bridge_h(c)
					
					h = h.view(self.dec_n_layers, batch_size, -1)
					c = c.view(self.dec_n_layers, batch_size, -1)
					hidden = (h, c)
			
		else:

			if self.enc_n_layers != self.dec_n_layers:

				if isinstance(hidden, tuple): # When using LSTM
					hidden = tuple([h[-self.n_directions:].repeat(self.dec_n_layers, 1, 1) for h in hidden])
				else: # GRU
					hidden = hidden[-self.n_directions:].repeat(self.dec_n_layers, 1, 1) 

		
		hidden = self._init_state(hidden)


		return hidden


	def _init_state(self, hidden):
		""" Initialize the decoder hidden state. """

		def _cat_directions(h):
			""" If the encoder is bidirectional, do the following transformation.
				(#directions * #layers, #batch, hidden_size) -> (#layers, #batch, #directions * hidden_size)
			"""
			if self.bidirectional:
				h = torch.cat([h[0:h.size(0):2], h[1:h.size(0):2]], 2)
			return h


		if hidden is None:
			return None
		if isinstance(hidden, tuple):
			hidden = tuple([_cat_directions(h) for h in hidden])
		else:
			hidden = _cat_directions(hidden)
		return hidden

