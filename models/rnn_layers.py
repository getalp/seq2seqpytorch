import random
import warnings

import torch
import torch.nn as nn
import torch.nn.functional as F

from .attention import Attention
from .pointer_generator import PointerGenerator


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class RNNEncoder(nn.Module):


	def __init__(self, vocab_size, embedding_dim, hidden_size, pad_id, n_layers=1, bidirectional=False, 
				 dropout=0, rnn_type='GRU', embedding=None, update_embedding=True):
	
		super(RNNEncoder, self).__init__()

		self.vocab_size = vocab_size
		self.embedding_dim = embedding_dim
		self.bidirectional = bidirectional
		self.n_directions = 2 if self.bidirectional else 1
		assert hidden_size % self.n_directions == 0
		self.hidden_size = hidden_size // self.n_directions
		self.pad_id = pad_id
		self.n_layers = n_layers
		self.dropout = dropout if self.n_layers > 1 else 0
		self.rnn_type = rnn_type
		
		self.embedding = nn.Embedding(self.vocab_size, self.embedding_dim, padding_idx=self.pad_id)
		if embedding is not None:
			self.embedding.weight = nn.Parameter(embedding)
		self.embedding.weight.requires_grad = update_embedding


		self.rnn = getattr(nn, self.rnn_type)(self.embedding_dim, self.hidden_size, self.n_layers, batch_first=True, 
											  bidirectional=self.bidirectional, dropout=self.dropout)
		self.input_dropout = nn.Dropout(p=self.dropout)



	def forward(self, src, src_lengths=None):

		embedded = self.embedding(src)
		embedded = self.input_dropout(embedded)

		if src_lengths is not None:
			embedded = nn.utils.rnn.pack_padded_sequence(embedded, src_lengths, batch_first=True)

		outputs, hidden = self.rnn(embedded)

		if src_lengths is not None:
			outputs, _ = nn.utils.rnn.pad_packed_sequence(outputs, batch_first=True)


		return outputs, hidden





class RNNDecoder(nn.Module):



	def __init__(self, vocab_size, embedding_dim, hidden_size, pad_id,
				 sos_id, eos_id, unk_id, max_length, n_layers=1,
				 dropout=0, rnn_type='GRU', use_attention=True, embedding=None,
				 update_embedding=True, pointer_gen=False, coverage=False,
				 attn_type='mlp', beam_size=1, beam_length_norm=0):

		super(RNNDecoder, self).__init__()

		self.vocab_size = vocab_size
		self.embedding_dim = embedding_dim
		self.hidden_size = hidden_size
		self.n_layers = n_layers
		self.dropout = dropout if self.n_layers > 1 else 0
		self.use_attention = use_attention
		self.pad_id = pad_id
		self.sos_id = sos_id
		self.eos_id = eos_id
		self.unk_id = unk_id
		self.max_length = max_length
		self.rnn_type = rnn_type
		self.coverage = coverage
		self.pointer_gen = pointer_gen
		self.beam_size = beam_size
		self.beam_length_norm = beam_length_norm


		self.embedding = nn.Embedding(self.vocab_size, self.embedding_dim, padding_idx=self.pad_id)
		if embedding is not None:
			self.embedding.weight = nn.Parameter(embedding)
		self.embedding.weight.requires_grad = update_embedding

		self.rnn = getattr(nn, self.rnn_type)(embedding_dim, self.hidden_size, n_layers, batch_first=True, dropout=self.dropout)
		self.input_dropout = nn.Dropout(p=self.dropout)

		if use_attention:
			self.attention = Attention(self.hidden_size, coverage=coverage, attn_type=attn_type)

		if pointer_gen:
			self.pointer_generator = PointerGenerator(self.hidden_size, self.embedding_dim, self.vocab_size)


		self.out = nn.Linear(self.hidden_size, self.vocab_size)




	def forward_step(self, trg, encoder_outputs=None, hidden=None, src_lengths=None, coverage=None, pg_src=None, max_oovs=0, **kwargs):

		batch_size = trg.size(0)
		trg_size = trg.size(1)

		embedded = self.embedding(trg)
		embedded = self.input_dropout(embedded)

		# Run the forward pass of the RNN.
		outputs, hidden = self.rnn(embedded, hidden)

		# Calculate the attention.
		if self.use_attention:
			outputs, attn_weights, context_v = self.attention(
				outputs.contiguous(),
				encoder_outputs,
				src_lengths=src_lengths,
				coverage = coverage
			)

		outputs = self.out(outputs.contiguous().view(-1, self.hidden_size))

		p_gen = None
		
		if self.pointer_gen:
	
			outputs = F.softmax(outputs, dim=1)

			if isinstance(hidden, tuple):
				hidden_ = hidden[0] #LSTM
			else:
				hidden_ = hidden #GRU

			outputs, p_gen = self.pointer_generator(context_v, hidden_, embedded, outputs, attn_weights, pg_src, max_oovs)

			outputs = torch.log(outputs).view(batch_size, trg_size, -1)
		
		else:
			outputs = F.log_softmax(outputs, dim=1).view(batch_size, trg_size, -1)

		return outputs, hidden, attn_weights, coverage, p_gen



	def forward(self, trg, encoder_hidden=None, encoder_outputs=None, src_lengths=None, 
		pg_src=None, max_oovs=0):

		src_size = encoder_outputs.size(1)
		batch_size  = encoder_outputs.size(0)
		max_length = trg.size(1)
		hidden = encoder_hidden

		if self.coverage:
			coverage = torch.zeros(batch_size, src_size).to(device) # coverage vector [Batch x SrcTimeSteps]
		else:
			coverage = None


		outputs = torch.zeros(batch_size, max_length, self.vocab_size + max_oovs).to(device)
		attn_weights = torch.zeros(batch_size, max_length, src_size).to(device)
		coverages = torch.zeros(batch_size, max_length, src_size).to(device)
		p_gens = torch.zeros(batch_size, max_length).to(device)


		if not (self.coverage or self.pointer_gen):
			
			decoder_input = trg
			outputs, hidden, attn_weights, coverages, p_gens = \
				self.forward_step(trg=decoder_input, encoder_outputs=encoder_outputs, hidden=hidden, src_lengths=src_lengths, coverage=coverage)

		else:

			for di in range(max_length):
				decoder_input = trg[:, di].unsqueeze(1) # we feed the groud truth tokens as the decoder input
				step_outputs, hidden, step_attn, coverage, p_gen = \
					self.forward_step(trg=decoder_input, encoder_outputs=encoder_outputs, hidden=hidden, src_lengths=src_lengths, coverage=coverage, pg_src=pg_src, max_oovs=max_oovs)
				outputs[:, di] = step_outputs.squeeze(1)
				attn_weights[:, di] = step_attn.squeeze(1)

				if coverage is not None:
					coverages[:, di] = coverage.squeeze(1)

					# Update the coverage attention.
					coverage = coverage + step_attn.squeeze(1)
				
				if p_gen is not None:
					p_gens[:, di] = p_gen.squeeze(1)

		return outputs, hidden, attn_weights, coverages, p_gens

