import torch
import torch.nn as nn


class Loss(nn.Module):
	
	def __init__(self, opts):
		super(Loss, self).__init__()
		self.opts = opts

	def coverage_loss(self, loss, attn_weights, coverage):
		coverage_loss = self.opts.coverage_weight * torch.mean(torch.mean(torch.sum(torch.min(attn_weights, coverage),2),1))
		loss += coverage_loss
		return loss

class NLLLoss(Loss):
	
	def __init__(self, opts):
		super(NLLLoss, self).__init__(opts)
		self.opts = opts
		self.criterion = nn.NLLLoss(ignore_index=self.opts.PAD_id)

	def forward(self, output, trg):
		loss = self.criterion(output.contiguous().view(-1, output.size(2)), trg.contiguous().view(-1))
		return loss
