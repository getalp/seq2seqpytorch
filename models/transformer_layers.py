import math

import torch.nn as nn
import torch
import torch.nn.functional as F

from utils.helper import clones
from .attention import MultiHeadedAttention

import numpy as np





class TransformerEncoder(nn.Module):
	'''TransformerEncoder is a stack of N encoder layers'''

	def __init__(self, vocab_size, layer, d_model, num_layers, dropout):
		super(TransformerEncoder, self).__init__()
		
		self.embedding = Embeddings(vocab_size, d_model)
		self.pos_embedding = PositionalEncoding(d_model, dropout)

		self.layers = clones(layer, num_layers)
		self.norm = nn.LayerNorm(d_model)
		
	def forward(self, src, mask=None):
		'''Pass the input (and mask) through each layer in turn.'''
		
		output = self.embedding(src)
		output = self.pos_embedding(output)

		for layer in self.layers:
			output = layer(output, mask)
		return self.norm(output)


class TransformerEncoderLayer(nn.Module):
	'''Encoder is made up of self-attn and feed forward (defined below)'''
	def __init__(self, d_model, n_head, d_ff, dropout):
		super(TransformerEncoderLayer, self).__init__()
		self.self_attn = MultiHeadedAttention(n_head, d_model, dropout=dropout)
		self.feed_forward = FeedForward(d_model, d_ff, dropout)

		self.norm1 = nn.LayerNorm(d_model)
		self.norm2 = nn.LayerNorm(d_model)
		self.dropout1 = nn.Dropout(dropout)
		self.dropout2 = nn.Dropout(dropout)

	def forward(self, src, mask):
		
		src_1 = self.norm1(src)
		src_1, attn = self.self_attn(src_1, src_1, src_1, mask=mask)
		src_1 = src + self.dropout1(src_1)

		src_2 = self.norm2(src_1)
		src_2 = self.feed_forward(src_2)
		src_2 = src_1 + self.dropout2(src_2)

		return src_2


class TransformerDecoder(nn.Module):
	'''TransformerDecoder is a stack of N decoder layers'''

	def __init__(self, vocab_size, layer, d_model, num_layers, dropout):
		super(TransformerDecoder, self).__init__()
		
		self.vocab_size = vocab_size
		self.embedding = Embeddings(vocab_size, d_model)
		self.pos_embedding = PositionalEncoding(d_model, dropout)

		self.layers = clones(layer, num_layers)
		self.norm = nn.LayerNorm(d_model)
		self.out = nn.Linear(d_model, vocab_size)


	def forward_step(self, trg, encoder_outputs=None, src_mask=None, trg_mask=None, **kwargs):
		output = self.embedding(trg)
		output = self.pos_embedding(output)

		for layer in self.layers:
			output, attn = layer(output, encoder_outputs, src_mask, trg_mask)
		
		output = self.norm(output)
		output = F.log_softmax(self.out(output), dim=-1)

		return output, None, attn, None, None

	def forward(self, trg, encoder_outputs, src_mask=None, trg_mask=None):

		output, _, _, _, _ = \
			self.forward_step(trg=trg, encoder_outputs=encoder_outputs, src_mask=src_mask, trg_mask=trg_mask)

		return output




class TransformerDecoderLayer(nn.Module):
	'''Decoder is made of self-attn, src-attn, and feed forward (defined below)'''
	
	def __init__(self, d_model, n_head, d_ff, dropout):
		super(TransformerDecoderLayer, self).__init__()
		
		self.self_attn = MultiHeadedAttention(n_head, d_model, dropout=dropout)
		self.src_attn = MultiHeadedAttention(n_head, d_model, dropout=dropout)
		self.feed_forward = FeedForward(d_model, d_ff, dropout)

		self.norm1 = nn.LayerNorm(d_model)
		self.norm2 = nn.LayerNorm(d_model)
		self.norm3 = nn.LayerNorm(d_model)
		self.dropout1 = nn.Dropout(dropout)
		self.dropout2 = nn.Dropout(dropout)
		self.dropout3 = nn.Dropout(dropout)

	def forward(self, trg, memory, src_mask, trg_mask):

		trg_1 = self.norm1(trg)
		trg_1, attn = self.self_attn(trg_1, trg_1, trg_1, mask=trg_mask)
		trg_1 = trg + self.dropout1(trg_1)

		trg_2 = self.norm2(trg_1)
		trg_2, attn = self.src_attn(trg_2, memory, memory, mask=src_mask)
		trg_2 = trg_1 + self.dropout2(trg_2)

		trg_3 = self.norm3(trg_2)
		trg_3 = self.feed_forward(trg_3)
		trg_3 = trg_2 + self.dropout3(trg_3)

		return trg_3, attn



class FeedForward(nn.Module):
	'''Implements FFN equation.'''
	def __init__(self, d_model, d_ff, dropout):
		super(FeedForward, self).__init__()
		self.w_1 = nn.Linear(d_model, d_ff)
		self.w_2 = nn.Linear(d_ff, d_model)
		self.dropout = nn.Dropout(dropout)

	def forward(self, x):
		return self.w_2(self.dropout(F.relu(self.w_1(x))))




class Embeddings(nn.Module):
	def __init__(self, vocab_size, d_model):
		super(Embeddings, self).__init__()
		self.lut = nn.Embedding(vocab_size, d_model)
		self.d_model = d_model

	def forward(self, x):
		return self.lut(x) * math.sqrt(self.d_model)



class PositionalEncoding(nn.Module):
	'''Implement the PE function.'''
	def __init__(self, d_model, dropout, max_len=5000):
		super(PositionalEncoding, self).__init__()
		self.dropout = nn.Dropout(p=dropout)
		
		# Compute the positional encodings once in log space.
		pe = torch.zeros(max_len, d_model)
		position = torch.arange(0., max_len).unsqueeze(1)
		div_term = torch.exp(torch.arange(0., d_model, 2) *
							 -(math.log(10000.0) / d_model))
		pe[:, 0::2] = torch.sin(position * div_term)
		pe[:, 1::2] = torch.cos(position * div_term)
		pe = pe.unsqueeze(0)
		self.register_buffer('pe', pe)
		
	def forward(self, x):
		x = x + self.pe[:, :x.size(1)]
		return self.dropout(x)



class _PositionalEncoding(nn.Module):
	'''Implement the PE function.'''
	def __init__(self, d_model, dropout, max_len=5000):
		super(_PositionalEncoding, self).__init__()
		
		self.pos_embedding = nn.Embedding(5000, d_model)
		self.dropout = nn.Dropout(p=dropout)
		
	def forward(self, x):
		positions = torch.arange(x.size(1)).to(x.device)
		embedding = x + self.pos_embedding(positions).expand_as(x)
		return self.dropout(embedding)

