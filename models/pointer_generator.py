
import torch
import torch.nn as nn
import torch.nn.functional as F

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class PointerGenerator(nn.Module):

	def __init__(self, hidden_size, embedding_dim, vocab_size):
		super(PointerGenerator, self).__init__()


		self.hidden_size = hidden_size
		self.embedding_dim = embedding_dim
		self.vocab_size = vocab_size

		self.wh = nn.Linear(hidden_size, 1) # for the context vector
		self.ws = nn.Linear(hidden_size, 1) # for the hidden state
		self.wx = nn.Linear(embedding_dim, 1) # for the input embedding




	def forward(self, context_v, hidden, embedded, outputs, attn_weights, pg_src, max_oovs):

		batch_size = outputs.size(0)

		assert pg_src.data.min() >=0 and pg_src.data.max() <= (self.vocab_size + max_oovs), 'recheck oov indices'

		wh_ = self.wh(context_v.contiguous().view(-1, context_v.size(2)))
		ws_ = self.ws(hidden[-1].contiguous().view(-1, hidden.size(2)))
		wx_ = self.wx(embedded.contiguous().view(-1, embedded.size(2)))

		p_gen = torch.sigmoid(wh_ + ws_ + wx_)

		outputs = p_gen * outputs
		_attn_weights = (1 - p_gen) * attn_weights.squeeze(1)

		if max_oovs > 0:
			empty_extra_vocab = torch.zeros(batch_size, max_oovs).to(device)
			outputs = torch.cat([outputs, empty_extra_vocab], 1)

		outputs = outputs.scatter_add(1, pg_src, _attn_weights)
		
		eps = 1e-12 # this is to prevent getting nan when applying torch.log, TODO: replace by clip
		outputs = outputs + eps

		return outputs, p_gen

