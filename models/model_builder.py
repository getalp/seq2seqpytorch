import torch

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

from models import RNNEncoder, RNNDecoder, Seq2seq, Bridge, Transformer


def build_seq2seq(src_vocab_size, trg_vocab_size, embedding_dim, hidden_size, enc_n_layers, dec_n_layers, 
		bidirectional, dropout, rnn_type, src_embedding, trg_embedding, max_length, pointer_gen, coverage, 
		attention_type, unk_id, pad_id, sos_id, eos_id, beam_size, beam_length_norm, opts):

		encoder = RNNEncoder(vocab_size=src_vocab_size,
							embedding_dim=embedding_dim,
							hidden_size=hidden_size,
							pad_id = pad_id, 
							n_layers=enc_n_layers,
							bidirectional=bidirectional,
							dropout=dropout,
							rnn_type=rnn_type,
							embedding=src_embedding)

		decoder = RNNDecoder(vocab_size=trg_vocab_size,
							embedding_dim=embedding_dim,
							hidden_size=hidden_size,
							pad_id=pad_id, 
							sos_id=sos_id,
							eos_id=eos_id,
							unk_id=unk_id,
							max_length=max_length,
							n_layers=dec_n_layers,
							dropout=dropout,
							rnn_type=rnn_type,
							embedding = trg_embedding,
							pointer_gen = pointer_gen,
							coverage = coverage, 
							attn_type = attention_type)

		if opts.share_vocab:
			encoder.embedding.weight = decoder.embedding.weight

		if opts.bridge:
			n_directions = 2 if bidirectional else 1
			assert hidden_size % n_directions == 0
			hidden_size = hidden_size // n_directions
			bridge = Bridge(hidden_size, n_directions, enc_n_layers, dec_n_layers, rnn_type)
		else:
			bridge = None

		seq2seq = Seq2seq(encoder, decoder, bridge).to(device)
		return seq2seq


def build_transformer(opts, src_vocab, trg_vocab, hidden_size, enc_n_layers, dec_n_layers, n_head, feedforward_size, dropout):
	
	transformer = Transformer(opts, src_vocab, trg_vocab, hidden_size, enc_n_layers, dec_n_layers, n_head, feedforward_size, dropout).to(device)

	return transformer
