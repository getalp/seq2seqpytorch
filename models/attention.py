import math

import torch
import torch.nn as nn
import torch.nn.functional as F


class Attention(nn.Module):

	def __init__(self, dim, coverage=False, attn_type='mlp'):
		super(Attention, self).__init__()

		self.dim = dim
		self.attn_type = attn_type

		if self.attn_type == "general":
			self.linear_in = nn.Linear(dim, dim, bias=False)
		elif self.attn_type == "mlp":
			self.linear_context = nn.Linear(dim, dim, bias=False)
			self.linear_query = nn.Linear(dim, dim, bias=True)
			self.v = nn.Linear(dim, 1, bias=False)
		
		# mlp needs is with bias
		out_bias = self.attn_type == "mlp"
		self.linear_out = nn.Linear(dim * 2, dim, bias=out_bias)


		if coverage:
			self.linear_cover = nn.Linear(1, dim, bias=False)


	def set_mask(self, mask):
		self.mask = mask



	def score(self, h_t, h_s):

		src_batch, src_len, src_dim = h_s.size()
		tgt_batch, tgt_len, tgt_dim = h_t.size()
		dim = self.dim

		if self.attn_type == 'mlp':
			wq = self.linear_query(h_t.view(-1, dim))
			wq = wq.view(tgt_batch, tgt_len, 1, dim)
			wq = wq.expand(tgt_batch, tgt_len, src_len, dim)

			uh = self.linear_context(h_s.contiguous().view(-1, dim))
			uh = uh.view(src_batch, 1, src_len, dim)
			uh = uh.expand(src_batch, tgt_len, src_len, dim)

			# (batch, t_len, s_len, d)
			wquh = torch.tanh(wq + uh)

			return self.v(wquh.view(-1, dim)).view(tgt_batch, tgt_len, src_len)

		elif self.attn_type in ["general", "dot"]:
			
			if self.attn_type == "general":
				h_t_ = h_t.view(tgt_batch * tgt_len, tgt_dim)
				h_t_ = self.linear_in(h_t_)
				h_t = h_t_.view(tgt_batch, tgt_len, tgt_dim)

			h_s_ = h_s.transpose(1, 2)
			
			return torch.bmm(h_t, h_s_)



	def forward(self, dec_output, enc_outputs, src_lengths=None, coverage=None):


		batch, source_l, dim = enc_outputs.size()
		batch_, target_l, dim_ = dec_output.size()


		if coverage is not None:
			coverage = coverage.view(-1).unsqueeze(1)
			enc_outputs += self.linear_cover(coverage).view_as(enc_outputs)
			enc_outputs = torch.tanh(enc_outputs)


		# compute attention scores
		align = self.score(dec_output, enc_outputs)

		if src_lengths is not None:
			mask = self.sequence_mask(src_lengths, max_len=align.size(-1))
			mask = mask.unsqueeze(1)  # Make it broadcastable.
			if align.size() != mask.size():
				mask = mask.repeat(align.size(0)//mask.size(0), 1, 1) # TODO: double check it
			align.masked_fill_(~mask, -float('inf'))


		# Softmax to normalize attention weights
		attn = F.softmax(align.view(batch*target_l, source_l), -1)
		attn = attn.view(batch, target_l, source_l)


		# each context vector c_t is the weighted average
		# over all the source hidden states
		context = torch.bmm(attn, enc_outputs)


		# concatenate
		concat_context = torch.cat([context, dec_output], 2).view(batch*target_l, dim*2)
		output = self.linear_out(concat_context).view(batch, target_l, dim)


		if self.attn_type in ["general", "dot"]:
			output = torch.tanh(output)

		return output, attn, context





	def sequence_mask(self, lengths, max_len=None):
		"""
		Creates a boolean mask from sequence lengths.
		"""
		batch_size = lengths.numel()
		max_len = max_len or lengths.max()
		return (torch.arange(0, max_len)
				.type_as(lengths)
				.repeat(batch_size, 1)
				.lt(lengths.unsqueeze(1)))



class MultiHeadedAttention(nn.Module):
	def __init__(self, n_head, d_model, dropout):
		super().__init__()
		
		self.d_model = d_model
		self.head_size = d_model // n_head
		self.n_head = n_head
		
		self.query_linear = nn.Linear(d_model, d_model)
		self.value_linear = nn.Linear(d_model, d_model)
		self.key_linear = nn.Linear(d_model, d_model)
		
		self.dropout = nn.Dropout(dropout)
		self.out = nn.Linear(d_model, d_model)

	def attention(self, query, key, value, mask=None, dropout=0.1):
		
		head_size = query.size(-1)
		scores = torch.matmul(query, key.transpose(-2, -1)) /  math.sqrt(head_size)
		
		if mask is not None:
			scores = scores.masked_fill(~mask.unsqueeze(1), float('-inf'))

		attn = F.softmax(scores, dim=-1)
		drop_attn = dropout(attn)
			
		context = torch.matmul(drop_attn, value)
		return context, attn

	def forward(self, query, key, value, mask=None):
		
		batch_size = query.size(0)
		query_len = query.size(1)
		key_len = key.size(1)
		
		# perform linear operation and split into n_head
		key = self.key_linear(key).view(batch_size, -1, self.n_head, self.head_size)
		query = self.query_linear(query).view(batch_size, -1, self.n_head, self.head_size)
		value = self.value_linear(value).view(batch_size, -1, self.n_head, self.head_size)
		
		# transpose to get dimensions batch_size * n_head * sl * d_model
		key = key.transpose(1,2)
		query = query.transpose(1,2)
		value = value.transpose(1,2)
		
		# calculate attention
		scores, attn = self.attention(query, key, value, mask, self.dropout)
		
		# concatenate n_head and put through final linear layer
		concat = scores.transpose(1,2).contiguous().view(batch_size, -1, self.d_model)
		
		output = self.out(concat)

		# top_attn = attn.view(batch_size, self.n_head, query_len, key_len)[:, 0, :, :].contiguous()
		top_attn = torch.mean(attn.view(batch_size, self.n_head, query_len, key_len), 1).contiguous()

		return output, top_attn