import argparse
import glob
import json
import re
import collections
import pandas as pd


import matplotlib.pyplot as plt
plt.switch_backend('agg')
import matplotlib.ticker as ticker



parser = argparse.ArgumentParser(description='Summarize Experiments in a folder')
parser.add_argument('-d', '--dir', help='directory of experiments folder', required=True)
parser.add_argument('-p', '--pattern', help='regex pattern to select specific folders', default='*', required=False)
args = parser.parse_args()

dir = args.dir
pattern = args.pattern





#HTML TEMPLATES##################################################################################

html = """
<html>
	<head>  
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
	</head>

	<body>
		{body}
	</body>
</html>
"""

accordion = """

<table style="width:100%;">
	<tr>
    <td colspan="2" style="width:90%;">
    	<div style="width:90%;">
			<canvas id="line-chart_train_loss" width="100" height="50"></canvas>
		</div>
    </td>
  </tr>
  <tr>
    <td style="width:49%;">
		<div style="width:100%;">
			<canvas id="line-chart_val_loss" width="100" height="50"></canvas>
		</div>
    </td>
    <td style="width:49%;">
		<div style="width:100%;">
			<canvas id="line-chart_val_acc" width="100" height="50"></canvas>
		</div>
    </td>
  </tr>
</table>

{plot}


<div id="accordion">
{accordion}
</div>

"""

card = """
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <span class="card-header" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
          {experiment_name}
        </span>
      </h5>
    </div>

    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">

		<table  style="widtd:100%;" cellspacing="10">
		  <tr>
		    <th >Config</th>
		    <th >Log</th>
		  </tr>
		  <tr>
		    <td valign="top">{config}</td>
		    <td valign="top">{log}</td>
		  </tr>
		</table>

       
      </div>
    </div>
  </div>
  </div>
"""

experiment_label = """
	<large class="float-sm-left">{experiment_name}</large> <small class="float-sm-right">Accuracy: {accuracy}</small>
"""

chart = """
<script>
var color = ["Blue ", "Red", "Orange", "Violet", "Green",  "Indigo", "Yellow "];

new Chart(document.getElementById("line-chart_{type}"), {{
  type: 'scatter',
  data: {{
    label: 'Scatter Dataset',
    datasets: [{chart_data}
    ]
  }},
  options: {{
	responsive: true,
    title: {{
      display: true,
      text: '{title}'
    }},
    tooltips: {{
            mode: 'index',
            intersect: false
    }},
	scales: {{
		xAxes: [{{
			display: true,
			scaleLabel: {{
				display: true,
				labelString: '{x_axis}'
			}}
		}}],
		yAxes: [{{
			display: true,
			scaleLabel: {{
				display: true,
				labelString: '{y_axis}'
			}}
		}}]
	}}    
  }}
}});
</script>

"""

chart_data = """
{{
    data: [{data}],
    label: "{label}",
    borderColor: color[{color}],
    fill: false,
    showLine: true
}},
"""

chart_data_point = """
{{ x: {x}, y: {y} }},
"""
#HTML TEMPLATES##################################################################################


#read the data from the experiments folder
configs_list = []
log_list = []

train_steps_list = []
val_steps_list = []

train_loss_list = []
val_loss_list = []
val_accuracy_list = []

patterns = pattern.split(',')
experiments = []
for pattern in patterns:
	experiments.extend(glob.glob(dir+'/'+pattern.strip()))

for experiment in experiments:
	
	train_steps = []
	val_steps = []

	train_loss = []
	val_loss = []
	val_accuracy = []

	config_file = glob.glob(experiment + '/*.json')[0]
	log_file = glob.glob(experiment + '/*.log')[0]
	


	train_pattern = r"ET: (?:.*)    Epoch: (?:\d+)    Progress: \((\d+) \d+\%\)    Loss: (\d+\.\d+)"
	val_pattern = r"Validation Iter: (\d+)    Loss: (\d+\.\d+)    Accuracy: (\d+\.\d+)"
	with open(log_file, 'r') as log_file_file:
		log = log_file_file.read()
		for train_match in re.finditer(train_pattern, log):
			iter, loss = train_match.groups()
			train_loss.append(loss)
			train_steps.append(iter)

		for val_match in re.finditer(val_pattern, log):
			iter, loss, acc = val_match.groups()
			val_loss.append(loss)
			val_steps.append(iter)
			val_accuracy.append(acc)


	train_steps_list.append(train_steps)
	val_steps_list.append(val_steps)

	train_loss_list.append(train_loss)
	val_loss_list.append(val_loss)
	val_accuracy_list.append(val_accuracy)
	
	with open(log_file, 'r') as text_data_file:
		log = text_data_file.read()
		log_list.append(log)
		iter, loss, acc = re.findall(r"Validation Iter: (\d+)    Loss: (\d+\.\d+)    Accuracy: (\d+\.\d+)", log)[-1]


	with open(config_file) as json_data_file:
		config = json.load(json_data_file)
		config['iter'] = val_steps[-1]
		config['loss'] = val_loss[-1]
		config['acc'] = val_accuracy[-1]

		configs_list.append(config)


#insert the data inside the html templates
card_list = ''
chart_val_acc = ''
chart_val_loss = ''
chart_train_loss = ''

for i, experiment in enumerate(experiments):
	
	log = log_list[i].replace('\n', '<br>')
	config = configs_list[i]
	
	train_loss = train_loss_list[i]
	train_steps = train_steps_list[i]
	val_loss = val_loss_list[i]
	val_steps = val_steps_list[i]
	val_accuracy = val_accuracy_list[i]

	
	data_points = ''
	for j, (x, y) in enumerate(zip(val_steps, val_accuracy)):
		data_points+=chart_data_point.format(x=x, y=y)
	chart_val_acc += chart_data.format(data=data_points, label=config['experiment_name'], color=i)

	data_points = ''
	for j, (x, y) in enumerate(zip(val_steps, val_loss)):
		data_points+=chart_data_point.format(x=x, y=y)
	chart_val_loss += chart_data.format(data=data_points, label=config['experiment_name'], color=i)

	data_points = ''
	for j, (x, y) in enumerate(zip(train_steps, train_loss)):
		data_points+=chart_data_point.format(x=x, y=y)
	chart_train_loss += chart_data.format(data=data_points, label=config['experiment_name'], color=i)

	config_str = ''
	for key, val in config.items():
		config_str+= '<b>{}</b>: {}<br>'.format(key, str(val))

	experiment = experiment_label.format(experiment_name=config['experiment_name'], accuracy=config['acc'])

	card_list += card.format(experiment_name=experiment, config=config_str, log=log).replace('One', str(i+1)) + '\n\n'


train_loss = chart.format(chart_data=chart_train_loss, title='Train Loss', type='train_loss', x_axis='Steps', y_axis='Loss')
val_loss = chart.format(chart_data=chart_val_loss, title='Validation Loss', type='val_loss', x_axis='Steps', y_axis='Loss')
val_acc = chart.format(chart_data=chart_val_acc, title='Validation Accuracy', type='val_acc', x_axis='Steps', y_axis='Accuracy')

accordion = accordion.format(accordion=card_list, plot=train_loss + val_loss + val_acc)
html = html.format(body=accordion)
html_file = open("summerized_exp.html", "w")
html_file.write(html)
html_file.close()
