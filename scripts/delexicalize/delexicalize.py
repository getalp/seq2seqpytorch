# Taken from: https://github.com/Itinte/natural_language_generation/blob/f48318a19cd0fa6008274bac46ac584945d27911/delexicalize.py
import numpy as np
import os
import random
import csv
import sys

def delex(input_sentence, target_sentence, slots): # Delexicalization function

	### INPUT SENTENCE KEY/VALUE DICTIONARY ###
	sentence_dict = {}
	for categories in input_sentence.split(','):
		pair = 0
		for element in categories.split('[ '):
			if pair % 2 == 0:
				key = element.strip()
				('key:', key)
			else:
				value = element[:-2]
				('value', value)
				sentence_dict[key] = value
			pair +=1
			(pair)

	### REBUILDING INPUT SENTENCE THROUGH DELEXICALIZATION ###
	new_input  = ''
	for categories in input_sentence.split(','):
		pair = 0
		delex = False
		for element in categories.split('[ '):
			if pair % 2 == 0:
				new_input_base = []
				key = element.strip()
				('key0', key)
				if key in slots:
					delex = True
				new_input_base.append(key)
			else:
				value = element[:-2]
				('value', value)
				if delex == True:
					new_input_base.append('x-'+key)
				else:
					new_input_base.append(value)
			pair+=1
		new_input += ' '.join(new_input_base)
		new_input += ', '

	### REBUILDING TARGET SENTENCE THROUGH DELEXICALIZATION ###
	for key in slots:
		if key in sentence_dict:
			if (key == 'area') and (sentence_dict[key] == 'riverside'):
				target_sentence = target_sentence.replace('Riverside', 'x-'+key)
			else:
				target_sentence = target_sentence.replace(sentence_dict[key], 'x-'+key)

	return sentence_dict, new_input, target_sentence


def relex(sent_dic, target_text): # Relexicalization function
	(sent_dic)
	for word in target_text.split():
		(word)
		if word[:2] == 'x-':
			key = word[2:]
			if (word[-1]=='.') or (word[-1]==','):
				key = key[:-1]
			if key in sent_dic:
				target_text = target_text.replace(word, sent_dic[key])

	return target_text
