import os
from delexicalize import delex, relex
import pickle
import sys

src_file = sys.argv[1]
trg_file = sys.argv[2]
output_path = sys.argv[3]


# slots = ['name1', 'headquarters1', 'founded1', 'industry1', 'industry2', 'industry3', 'type1', 'key_people1', 'key_people2', 'key_people3', 'products1', 'products2', 'products3', 'number of employees1', 'founders1', 'founders2', 'founders3', 'founder1', 'founder2', 'founder3', 'owner1', 'owner2', 'owner3', 'defunct1', 'services1', 'services2', 'services3']
slots = ['name1', 'founded1', 'key_people1', 'key_people2', 'key_people3', 'founders1', 'founders2', 'founders3', 'founder1', 'founder2', 'founder3', 'owner1', 'owner2', 'owner3', 'defunct1']


def delexicalize(src_file, trg_file):

	delex_dic = []
	delex_src = []
	delex_trg = []

	with open(src_file) as fs, open(trg_file) as ft:
		for line_i, (src_line, trg_line) in enumerate(zip(fs, ft)):
			src_line, trg_line = src_line.strip(), trg_line.strip()
			slot_dict, src_text, trg_text = delex(src_line, trg_line, slots)
			delex_dic.append(slot_dict)
			delex_src.append(src_text)
			delex_trg.append(trg_text)

	src_f = os.path.basename(src_file)[:-4]
	trg_f = os.path.basename(trg_file)[:-4]

	src_file_out = os.path.join(output_path, 'delexicalized_%s.txt'%src_f)
	trg_file_out = os.path.join(output_path, 'delexicalized_%s.txt'%trg_f)

	with open(src_file_out, 'w') as fs, open(trg_file_out, 'w') as ft:

		for line_i, (src_line, trg_line) in enumerate(zip(delex_src, delex_trg)):
			src_line, trg_line = src_line.strip(), trg_line.strip()
			fs.write(src_line+'\n')
			ft.write(trg_line+'\n')

	delex_file_out = os.path.join(output_path, 'delex_dic_%s.pkl'%src_f)
	with open(delex_file_out, 'wb') as fd:
		pickle.dump(delex_dic, fd, protocol=pickle.HIGHEST_PROTOCOL)






delexicalize(src_file, trg_file)




