# coding: utf-8
from __future__ import unicode_literals, print_function, division
import os
import logging
import time
import math
import unicodedata
import logging
from shutil import copyfile
import argparse

import torch
import torch.nn as nn
from torch import optim

from tensorboardX import SummaryWriter

from torchtext_lite import torchtext_lite as torchtext
from utils import data_util
from utils import checkpoint_util
from utils import arg_util

from models.model_builder import build_seq2seq, build_transformer
from models import NLLLoss

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
logger = logging.getLogger(__name__)

#TODO: log and print simultaneously
#TODO: implement earlystop
class Trainer:

	def __init__(self, opts, model, optimizer, lr_scheduler, loss, train_dataset, val_dataset):
		
		self.opts = opts
		self.model = model
		self.optimizer = optimizer
		self.lr_scheduler = lr_scheduler
		self.loss = loss
		
		self.train_dataset = train_dataset
		self.val_dataset = val_dataset

		tb_dir = os.path.join(opts.experiment_dir, opts.experiment_name) + '/tb/'
		self.writer = SummaryWriter(logdir=tb_dir)



	def accuracy(self, output, trg):
		
		outputs = output['log_probs']

		labels = torch.argmax(outputs.contiguous().view(-1, outputs.size(2)), dim=1)
		accuracy = torch.sum(trg.contiguous().view(-1) == labels).item() / (len(trg.contiguous().view(-1)) * 1.0)

		return accuracy


	def forward_step(self, batch, mode='train'):
		
		src, src_len = batch.src
		trg, trg_len = batch.trg
		trg_in = trg[:,:-1]
		trg_out = trg[:,1:]
		src_raw = batch.src_raw
		trg_raw = batch.trg_raw

		if self.opts.pointer_gen:
			pg_src, pg_trg, max_oovs, _ = self.train_dataset.pg_batch(src, src_raw, trg_out, trg_raw)
			trg_out = pg_trg
		else:
			pg_src, pg_trg, max_oovs = None, None, 0

		output = self.model(src, src_len, trg_in, pg_src=pg_src, max_oovs=max_oovs)
		log_probs = output['log_probs']
		attn_weights = output['attn_weights']
		coverage = output['coverage']

		loss = self.loss(log_probs, trg_out)
		if self.opts.coverage:
			loss += self.loss.coverage_loss(loss, attn_weights, coverage)

		if mode == 'validate':
			accuracy = self.accuracy(output, trg_out)
			return loss, accuracy
		else:
			return loss


	def train_step(self, batch):

		loss = self.forward_step(batch, mode='train')

		self.model.zero_grad()
		loss.backward()
		if self.opts.clip_grad:
			nn.utils.clip_grad_norm_(self.model.parameters(), self.opts.clip_grad)
		self.optimizer.step()

		loss = loss.data.item()

		return loss


	def validation_step(self, batch):

		self.model.eval()
		loss, accuracy = self.forward_step(batch, mode='validate')
		loss = loss.data.item()
		self.model.train()
		return loss, accuracy


	def train(self, mode):

		if mode == 'resume':
			# Load checkpoint
			checkpoint = checkpoint_util.load_checkpoint(self.opts)

			self.model.load_state_dict(checkpoint['model_state_dict'], strict=False)
			if not self.opts.coverage: self.optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
			self.lr_scheduler.load_state_dict(checkpoint['lr_scheduler_state_dict'])
			self.global_step = checkpoint['global_step']
			self.train_loss_history = checkpoint['train_loss_history']
			self.start_time = checkpoint['start_time']

		else:
			self.global_step = 1
			self.train_loss_history = []
			self.start_time = time.time()


		for batch, iter, epoch, epoch_iter, total_batches in self.data_iterator(mode='train'):

			loss = self.train_step(batch)
			self.writer.add_scalar('train/loss', loss, iter)

			self.train_loss_history.append(loss)
			# print('Epoch: {}    iteration: {}    epoch iteration: {}/{},    batch loss: {}'.format(epoch, iter, epoch_iter, total_batches, loss), end="\r")
			print('Epoch: {}    Iteration: {}    Epoch iteration: {}/{}'.format(epoch, iter, epoch_iter, total_batches), end="\r")

			self.print_loss(iter, epoch)
			self.validation(iter)
			self.save_checkpoint(iter)



	def validation(self, iter):

		if (iter != self.global_step) and (iter % self.opts.evaluate_model_every == 0):
		
			total_loss = 0
			total_accuracy = 0
			loss_norm_term = 0

			for batch in self.data_iterator(mode='validate'):
				loss, accuracy = self.validation_step(batch)

				total_loss += loss
				total_accuracy += accuracy
				loss_norm_term +=1

			total_loss /= loss_norm_term
			total_accuracy /= loss_norm_term

			self.writer.add_scalar('validation/loss', total_loss, iter)
			self.writer.add_scalar('validation/accuracy', total_accuracy, iter)


			val_loss_msg = 'Validation Iter: %d    Loss: %.4f    Accuracy: %.4f' % (iter, total_loss, total_accuracy)
			print(val_loss_msg)
			logger.info(val_loss_msg)

			# apply learning rate scheduler (decay)
			if iter >= self.opts.lr_scheduler_start_at:
				self.lr_scheduler.step(total_loss)
			self.get_learning_rate(self.optimizer)


	def data_iterator(self, mode='train'):

		if mode == 'train':
			batch_iterator = torchtext.BucketIterator(dataset=self.train_dataset.dataset, batch_size=self.opts.batch_size, sort=True, shuffle=True, 
			sort_within_batch=True,  sort_key=lambda x: len(x.src), device=device, repeat=True)
		else:
			batch_iterator = torchtext.BucketIterator(dataset=self.val_dataset.dataset, batch_size=self.opts.batch_size, sort=False, shuffle=False, 
				repeat=False, sort_key=lambda x: len(x.src), device=device)


		batch_generator = batch_iterator.__iter__()

		total_batches = len(batch_iterator)

		for iter in range(self.global_step, self.opts.total_nb_iter + 1):

			try:
				batch = next(batch_generator)
			except StopIteration: return

			epoch = (iter//total_batches)+1 # batch_iterator.epoch+1 
			epoch_iter = batch_iterator.epoch_iterations

			if mode == 'train':
				yield batch, iter, epoch, epoch_iter, total_batches
			else:
				yield batch


	def get_learning_rate(self, optimizer):
		for param_group in optimizer.param_groups:
			lr_msg = 'Current learning rate is: {}'.format(param_group['lr'])
			print(lr_msg)
			logger.info(lr_msg)



	def print_loss(self, iter, epoch):
			
		if (iter != self.global_step) and (iter % self.opts.print_loss_every == 0):
			loss_avg = sum(self.train_loss_history[-self.opts.print_loss_every:])/len(self.train_loss_history[-self.opts.print_loss_every:])

			print_summary = 'ET: %s    Epoch: %s    Progress: (%d %d%%)    Loss: %.4f' % (self.time_since(self.start_time, iter/self.opts.total_nb_iter), epoch, iter, (iter/self.opts.total_nb_iter)*100, loss_avg)
			print(print_summary)
			logger.info(print_summary)


	def save_checkpoint(self, iter):

		if (iter != self.global_step) and (iter % self.opts.save_model_every == 0):
			last_loss = self.train_loss_history[-1]
			self.global_step = iter
			checkpoint_util.save_checkpoint(self.opts, self.model, self.optimizer, self.lr_scheduler,
				last_loss, self.global_step, self.train_loss_history, self.start_time)
			

	def time_since(self, since, percent):

		def as_minutes(s):
			m = math.floor(s / 60)
			s -= m * 60
			return '%dm %ds' % (m, s)

		now = time.time()
		s = now - since
		es = s / (percent)
		rs = es - s
		return '%s (- %s)' % (as_minutes(s), as_minutes(rs))





if __name__ == '__main__':

	opts, config = arg_util.trainer_args()
	
	if not opts.experiment_dir:
		opts.experiment_dir = './experiments/'
	experiment_dir = os.path.join(opts.experiment_dir, opts.experiment_name)
	if not os.path.exists(experiment_dir): os.makedirs(experiment_dir)
	else:
		if opts.mode == 'train':
			raise Exception('%s already exists' % experiment_dir)

	if opts.mode == 'train':
		config_file_name = os.path.basename(opts.config_file)
		copyfile(opts.config_file, os.path.join(experiment_dir, config_file_name))

	# Load data
	train_dataset, val_dataset = data_util.load_data(opts.train_file, opts.val_file, opts.vocab_size, opts.src_max_length, opts.trg_max_length, opts.share_vocab, opts.src_vocab, opts.trg_vocab, split_level=opts.split_level)
	assert (train_dataset.source_lang.PAD_id == train_dataset.target_lang.PAD_id) and (train_dataset.source_lang.UNK_id == train_dataset.target_lang.UNK_id) 

	opts.PAD_id = train_dataset.target_lang.PAD_id
	opts.UNK_id = train_dataset.target_lang.UNK_id
	opts.SOS_id = train_dataset.target_lang.SOS_id
	opts.EOS_id = train_dataset.target_lang.EOS_id


	if opts.pointer_gen:
		assert opts.pointer_gen == opts.share_vocab, 'when using pointer_gen, share_vocab has to be set to true.'
		assert train_dataset.target_lang.n_words == train_dataset.source_lang.n_words


	# Load pretrained embeddings
	src_emb_vect = None
	trg_emb_vect = None

	if opts.mode == 'train':
		if opts.src_emb:
			src_emb_vect = train_dataset.dataset.fields['src'].vocab.load_embeddings(opts.src_emb)
		if opts.trg_emb:
			trg_emb_vect = train_dataset.dataset.fields['trg'].vocab.load_embeddings(opts.trg_emb)


	# Logger
	log_format = '[%(asctime)s %(levelname)s] %(message)s'
	logging.basicConfig(filename='{}/{}.log'.format(experiment_dir, opts.experiment_name), format=log_format, filemode='a', level=logging.INFO)
	logger = logging.getLogger('seq2seq')



	# Set manual seed
	if opts.manual_seed:
		torch.manual_seed(opts.manual_seed)
		torch.cuda.manual_seed_all(opts.manual_seed)
		torch.backends.cudnn.deterministic = True

	if opts.transformer == True:
		model = build_transformer(opts,
			train_dataset.source_lang.n_words, 
			train_dataset.target_lang.n_words, 
			opts.hidden_size, 
			opts.enc_n_layers, 
			opts.dec_n_layers, 
			opts.n_head, 
			opts.feedforward_size, 
			opts.dropout)
	else:
		model = build_seq2seq(train_dataset.source_lang.n_words, 
			train_dataset.target_lang.n_words,
			opts.embedding_dim,
			opts.hidden_size,
			opts.enc_n_layers,
			opts.dec_n_layers, 
			opts.bi_rnn,
			opts.dropout,
			opts.rnn_type,
			src_emb_vect,
			trg_emb_vect, 
			opts.trg_max_length,
			opts.pointer_gen,
			opts.coverage, 
			opts.attention_type, 
			opts.UNK_id,
			opts.PAD_id,
			opts.SOS_id,
			opts.EOS_id,
			opts.beam_size, 
			opts.beam_length_norm,
			opts)


	print(model.summary())
	logger.info(model.summary())

	print(config.summary())
	logger.info(config.summary())


	optimizer = optim.Adam(model.parameters(), lr=opts.lr, weight_decay=opts.weight_decay)
	lr_scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=opts.lr_scheduler_factor, patience=opts.lr_scheduler_patience, verbose=False)
	loss = NLLLoss(opts)

	trainer = Trainer(opts, model, optimizer, lr_scheduler, loss, train_dataset, val_dataset)
	trainer.train(opts.mode)