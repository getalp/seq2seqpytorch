import os
from utils import data_util
import argparse



# http://statmt.org/wmt14/translation-task.html#download

parser = argparse.ArgumentParser(description='Preprocessor')
parser.add_argument('--src_train', help='train source file', required=True)
parser.add_argument('--trg_train', help='train target file', required=True)
parser.add_argument('--src_val', help='validation source file', required=True)
parser.add_argument('--trg_val', help='validation target file', required=True)
parser.add_argument('--save_path', help='location to store files', required=True)
parser.add_argument('--save_name', help='name output train and val files', required=True)
parser.add_argument('--src_max_length', type=int, help='source max length of sequences', required=True)
parser.add_argument('--trg_max_length', type=int, help='target max length of sequences', required=True)
parser.add_argument('--share_vocab', help='share same vocabulary between src and trg', required=False, default=False, action='store_true')
parser.add_argument('--split_level', help='split text at word or char level', default='word', required=False)
parser.add_argument('--vocab_size', type=int, help='vocabulary size', required=False, default=50000)

args = parser.parse_args()

save_path = args.save_path
save_name = args.save_name
src_train = args.src_train
trg_train = args.trg_train
src_val = args.src_val
trg_val = args.trg_val
src_max_length = args.src_max_length
trg_max_length = args.trg_max_length
share_vocab = args.share_vocab
split_level = args.split_level
vocab_size = args.vocab_size

if not os.path.exists(save_path):
	os.makedirs(save_path)

data_util.prepare_data(src_train, trg_train, src_val, trg_val, save_path, save_name, src_max_length, trg_max_length, share_vocab, vocab_size, split_level)




'''
python preprocessor.py --train_name train_wmt --val_name val_wmt --src_train ../stacked_seq2seq_data/wmt14/WMT14.fr-en.fr --trg_train ../stacked_seq2seq_data/wmt14/WMT14.fr-en.en --src_val ../stacked_seq2seq_data/wmt14/ntst1213.fr-en.fr --trg_val ../stacked_seq2seq_data/wmt14/ntst14.fr-en.en --save_path data/ --max_length 50
python preprocessor.py --train_name train_torchtext --val_name val_torchtext --src_train ../stacked_seq2seq_data/data/train_french --trg_train ../stacked_seq2seq_data/data/train_english --src_val ../stacked_seq2seq_data/data/val_french --trg_val ../stacked_seq2seq_data/data/val_english --save_path data/ --max_length 50
'''