# Details of parameters in the configuration file

Note: if you want to use the tranformer model, you don't need to have LSTM/GRU attributes, likewise if you want to use LSTM or GRU, you can ignore the transformer attributes.

| Parameter             | type        | Possible values            | Explanation                                                            |
|-----------------------|-------------|----------------------------|------------------------------------------------------------------------|
| experiment_name       | experiment  | my_experiment              | name of your experiment                                                |
| experiment_dir        | experiment  | ./experiments              | directory to store your experiment files                               |
|                       |             |                            |                                                                        |
| train_file            | data        | data/e2e/e2e_train         | training file created by preprocessor.py                               |
| val_file              | data        | data/e2e/e2e_val           | validation file created by preprocessor.py                             |
| src_vocab             | data        | data/e2e/e2e_src_vocab.txt | source vocab created by preprocessor.py                                |
| trg_vocab             | data        | data/e2e/e2e_trg_vocab.txt | target vocab created by preprocessor.py                                |
| split_level           | data        | word, char                 | tokenize at word or character level                                    |
| share_vocab           | data        | true, false                | whether to share vocab between source and target or not                |
| vocab_size            | data        | 50000                      | vocabulary size                                                        |
| src_max_length        | data        | 50                         | maximum length of source sequences                                     |
| trg_max_length        | data        | 50                         | maximum length of target sequences                                     |
|                       |             |                            |                                                                        |
| rnn_type              | LSTM/GRU    | LSTM, GRU                  | type of RNN cell                                                       |
| bi_rnn                | LSTM/GRU    | true, false                | whether to use bi-directional rnn for the encoder or not               |
| bridge                | LSTM/GRU    | true, false                | whether to pass the last encoder hidden state to the decoder or not    |
| attention_type        | LSTM/GRU    | mlp, general, dot          | type of attention mechanism                                            |
| pointer_gen           | LSTM/GRU    | true, false                | use pointer generator or not                                           |
| coverage              | LSTM/GRU    | true, false                | whether to use coverage mechanism or not                               |
| coverage_weight       | LSTM/GRU    | 1                          | weight of coverage mechanism                                           |
|                       |             |                            |                                                                        |
| transformer           | transformer | true, false                | wether to use transformer or not                                       |
| n_head                | transformer | 8                          | number of self attention heads                                         |
| feedforward_size      | transformer | 2048                       | size of the feed forward module                                        |
|                       |             |                            |                                                                        |
| enc_n_layers          | model       | 1                          | number of encoder layers                                               |
| dec_n_layers          | model       | 1                          | number of decoder layers                                               |
| hidden_size           | model       | 1000                       | number of hidden units (for rnn, transformer, and attention)           |
| embedding_dim         | model       | 620                        | word embedding dimension                                               |
| dropout               | model       | 0.2                        | dropout value                                                          |
|                       |             |                            |                                                                        |
| print_loss_every      | training    | 10                         | print loss every n iterations                                          |
| save_model_every      | training    | 2000                       | save model every n iterations                                          |
| evaluate_model_every  | training    | 1000                       | evaluate model on validation data every n iterations                   |
| total_nb_iter         | training    | 128000                     | total number of training iterations                                    |
| batch_size            | training    | 128                        | number of sequences in a batch                                         |
| lr                    | training    | 2e-4                       | learning rate value                                                    |
| lr_scheduler_factor   | training    | 0.5                        | reduce learning rate by this value                                     |
| lr_scheduler_start_at | training    | 4000                       | start reducing only after this many steps                              |
| lr_scheduler_patience | training    | 10                         | if validation loss didn't reduce for n times, reduce the learning rate |
| weight_decay          | training    | 0.2                        | weight decay for the optimizer                                         |
| clip_grad             | training    | 2                          | clip the norm of gradients at this value                               |
| manual_seed           | training    | 1234                       | seed value for reproducibility                                         |
|                       |             |                            |                                                                        |
| beam_size             | decoding    | 4                          | beam size, 1 means greedy decoding                                     |
| beam_length_norm      | decoding    | 0.5                        | beam length normalization (penalty). between 0 - 1                     |
| UNK_rep               | decoding    | true, false                | whether to replace oov words or not                                    |
