# Seq2SeqPy

Seq2SeqPy is a lightweight toolkit for sequence-to-sequence modeling that prioritizes simplicity and ability to customize the standard architectures/models easily. It supports several known models such as Recurrent Neural Networks, Pointer Generator Networks, and transformer model.
&nbsp;

## Setup

### Clone the repository

```bash
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/getalp/seq2seqpytorch
```

### Install the requirements

```bash
pip install -r requirements.txt
```

## Quick starting guide

#### 1. Data prerpocessing

```bash
DATA_PATH=data/e2e
python preprocessor.py [-h] --src_train $DATA_PATH/src_train.txt \
                            --trg_train $DATA_PATH/trg_train.txt \
                            --src_val  $DATA_PATH/src_validation.txt \ 
                            --trg_val  $DATA_PATH/trg_validation.txt \
                            --save_path $DATA_PATH \
                            --save_name e2e \
                            --src_max_length 100 \
                            --trg_max_length 100 \
                            --vocab_size 50000
```

This command will create the following 4 files inside $DATA_PATH directory:
> e2e_train, e2e_val, e2e_src_vocab.txt, e2e_trg_vocab.txt

#### 2. Training

```bash
python trainer.py [-h] -c config_file.json
```

#### 3. Resume training

```bash
python trainer.py -c config_file.json --resume
```

#### 4. Generating text

```bash
DATA_PATH=data/e2e
python generator.py [-h] -c config_file.json -g $DATA_PATH/src_test.txt -o output.txt
```

&nbsp;

## Configuration file details

You can find configuration file details [here](https://gricad-gitlab.univ-grenoble-alpes.fr/getalp/seq2seqpytorch/tree/master/configs)

## Notes

* Currently it only supports training on a single GPU
