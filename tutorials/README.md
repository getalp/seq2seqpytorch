# Tutorials

## Tutorial 1: integrating BERT

**Step 1: Change the source field from TextField to BertField**  
*File: utils/data_utils.py*  
*Line: 66*  

```diff
- src_field = torchtext.TextField()
+ src_field = torchtext.BertField()
```

**Step 2: Remove vocabulary builder for src_field**  
Since BERT has its own tokenizer and vocabulary builder, we need to
remove all related lines.  
*File: utils/data_utils.py*  
*Line: 117 & 122*  

```diff
- src_field.vocab = trg_field.vocab
- src_field.build_vocab(voc_dataset ... )
```

**Step 3: Replace the src_field vocab with BERTs vocab**
*File: utils/data_utils.py*  
*Line: 189 to 193 & 235 to 239*  

```diff
- source_lang.word2index = src_field.vocab.word2index
- source_lang.index2word = src_field.vocab.index2word
- source_lang.n_words = len(source_lang.word2index)
- source_lang.PAD_id = source_lang.word2index[torchtext.PAD_token]
- source_lang.UNK_id = source_lang.word2index[torchtext.UNK_token]

+ source_lang.word2index = bert_tokenizer.convert_tokens_to_ids
+ source_lang.index2word = bert_tokenizer.convert_ids_to_tokens
+ source_lang.n_words = len(bert_tokenizer.vocab)
+ source_lang.PAD_id = bert_tokenizer.pad_token_id
+ source_lang.UNK_id = bert_tokenizer.unk_token_id
```

**Step 4: Replace default embedding with BERT embedding in the encoder**  
*File: models/rnn_layers.py*  
*Line: 1*  

```diff
+ from transformers import BertModel
+ bert = BertModel.from_pretrained('bert-base-uncased’)
```

*Line: 121*

```diff
- embedded = self.embedding(src)
+ embedded = bert(src)[0]
```

**Step 5: Update the configuration file**
In the configuration file, the embedding_dim should be changed to 768 in order to make your seq2seq model compatible with Bert model.

## Tutorial 2: adding second encoder

comming soon

## Tutorial 3: adding second decoder with multiple attention mechanisms

comming soon
