
import torch
import numpy as np
from torchtext_lite import torchtext_lite as torchtext


def decode(log_probs, attn_weights, source_sentences, target_lang, UNK_rep, oovs, beam_size=1, split_level='word'):

	if split_level: torchtext.split_level=split_level

	UNK_id = target_lang.UNK_id
	EOS_id = target_lang.EOS_id
	PAD_id = target_lang.PAD_id

	def _decode_token(token_i, attention, source_sentence, oov=None):

		if token_i == PAD_id:
			return ''

		if token_i == UNK_id and UNK_rep:
			score, idx = attention.max(0)
			if idx.item() < len(source_sentence):
				token = source_sentence[idx.item()]
			else:
				token = target_lang.index2word[token_i]
		else:
			try:
				token = target_lang.index2word[token_i]
			except:
				article_oov_idx = token_i - target_lang.n_words
				token = oov[article_oov_idx]

		return token



	'''
	log_probs = log_probs.topk(1)[1]
	decoded_words = [[_decode_token(log_probs[si][di].item(), attn_weights[si][di], source_sentences[si],  oovs[si] if oovs else None) for di, dec in enumerate(sequence)] for si, sequence in enumerate(log_probs)]
	'''

	lengths = np.array([log_probs.size(1)] * log_probs.size(0))
	decoded_symbols = []

	for di in range(log_probs.size(1)):

		if beam_size > 1:
			symbols = log_probs[:, di]
		else:
			step_output = log_probs[:, di, :]
			symbols = step_output.topk(1)[1]

		decoded_symbols.append(symbols)


		eos_batches = symbols.data.eq(EOS_id)
		if eos_batches.dim() > 0:
			eos_batches = eos_batches.cpu().view(-1).numpy()
			update_idx = ((lengths > di) & eos_batches) != 0
			lengths[update_idx] = len(decoded_symbols)

	decoded_words = [[_decode_token(decoded_symbols[di][bi].item(), attn_weights[bi][di], torchtext.tokenizer(source_sentences[bi]), oovs[bi] if oovs else None) for di in range(length)] for bi, length in enumerate(lengths)]


	return decoded_words


'''
def get_sentence(seq):
	if EOS_id in seq: return seq[:seq.index(EOS_id)+1]
	else: return seq
'''		