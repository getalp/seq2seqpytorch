import argparse
import logging
from . import config_util

logger = logging.getLogger(__name__)


def parse_args(opts, args):
	for arg in vars(args):
		if getattr(args, arg):
			setattr(opts, arg, getattr(args, arg))

def trainer_args():

	parser = argparse.ArgumentParser(description='Trainer')
	parser.add_argument('-c', '--config_file', help='configuration file', required=True)
	
	parser.add_argument('-t', '--train_file', help='train file path', required=False)
	parser.add_argument('-v', '--val_file', help='val file path', required=False)
	parser.add_argument('--resume', help='resume model and train', required=False, action='store_true')
	parser.add_argument('--src_vocab', help='src vocabulary file', required=False)
	parser.add_argument('--trg_vocab', help='src vocabulary file', required=False)
	parser.add_argument('--batch_size', help='batch size', type=int, required=False)
	parser.add_argument('--src_emb', help='path to pretrained source embeddings, e.g., glove.840B.300d', required=False)
	parser.add_argument('--trg_emb', help='path to pretrained target embeddings, e.g., glove.840B.300d', required=False)

	args = parser.parse_args()

	config_file = args.config_file
	config = config_util.Config(config_file)
	
	print(config.summary())
	logger.info(config.summary())
	
	opts = config.load()
	parse_args(opts, args)

	if args.resume: opts.mode = 'resume'
	else: opts.mode = 'train'

	return opts, config
	


def generator_args():

	parser = argparse.ArgumentParser(description='Generator')
	parser.add_argument('-c', '--config_file', help='configuration file', required=True)
	
	parser.add_argument('-g', '--gen_file', help='txt file to generate from', required=False)
	parser.add_argument('-o', '--output_file', help='file to save generated sequences', required=False)
	
	parser.add_argument('--src_vocab', help='src vocabulary file', required=False)
	parser.add_argument('--trg_vocab', help='src vocabulary file', required=False)
	parser.add_argument('--batch_size', help='batch size', type=int, required=False)
	parser.add_argument('--beam_size', help='beam size', type=int, required=False)
	parser.add_argument('--beam_length_norm', default=None, type=float, help='beam length norm between 0.0 and 1.0')
	parser.add_argument('--visualize', help='visualize the attention map', required=False, action='store_true')
	parser.add_argument('-i', '--interactive', help='Type in input sequences interactively', required=False, action='store_true')
	parser.add_argument('--checkpoint', help='path to checkpoint file', default=None, required=False)


	args = parser.parse_args()

	config_file = args.config_file
	config = config_util.Config(config_file)
	
	opts = config.load()
	parse_args(opts, args)

	return opts, config