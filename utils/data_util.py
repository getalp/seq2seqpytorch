from __future__ import unicode_literals, print_function
import unicodedata
import re
import time
import math
import random
from io import open
import logging
import pickle
#import cPickle as pickle
import os
# import sys
# reload(sys)
# sys.setdefaultencoding('utf-8')

import numpy as np
import torch

from torchtext_lite import torchtext_lite as torchtext
from .attribute_dic import AttrDict

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
logger = logging.getLogger(__name__)


# https://stackoverflow.com/questions/31468117/python-3-can-pickle-handle-byte-objects-larger-than-4gb



class Dataset(torchtext.Dataset):

	def __init__(self, src_path, trg_path, fields, src_max_length=None, trg_max_length=None, **kwargs):

		if not isinstance(fields[0], (tuple, list)):
			fields = [('src', fields[0]), ('src_raw', fields[1]), ('trg', fields[2]), ('trg_raw', fields[3])]


		examples = []
		with open(src_path, encoding='utf-8') as src_file, open(trg_path, encoding='utf-8') as trg_file:
			for line_i, (src_line, trg_line) in enumerate(zip(src_file, trg_file)):
				src_line, trg_line = torchtext.tokenizer(src_line.strip()), torchtext.tokenizer(trg_line.strip())
				print('Reading line #: ', line_i, end='\r')

				if src_max_length is not None:
					src_line = src_line[:src_max_length]
					src_line = torchtext.detokenizer(src_line)

				if trg_max_length is not None:
					trg_line = trg_line[:trg_max_length]
					trg_line = torchtext.detokenizer(trg_line)
					


				if src_line != '' and trg_line != '':
					examples.append(torchtext.Example(
						[src_line, src_line, trg_line, trg_line], fields))

		super(Dataset, self).__init__(examples, fields, **kwargs)




	
def generate_fields():

	src_field = torchtext.TextField()
	

	src_raw_field = torchtext.RawField()
	
	if hasattr(src_raw_field, 'is_target'):
		src_raw_field.is_target = False

	trg_field = torchtext.TextField(add_sos=True, add_eos=True)


	trg_raw_field = torchtext.RawField()
	
	if hasattr(trg_raw_field, 'is_target'):
		trg_raw_field.is_target = False



	return src_field, src_raw_field, trg_field, trg_raw_field




def prepare_data(src_train, trg_train, src_val, trg_val, save_path, save_name, src_max_length=None, trg_max_length=None, share_vocab=False, vocab_size=50000, split_level='word'):

	if split_level: torchtext.split_level=split_level

	src_field, src_raw_field, trg_field, trg_raw_field = generate_fields()
	fields = [('src', src_field), ('src_raw', src_raw_field), ('trg', trg_field), ('trg_raw', trg_raw_field)]


	# Generating torchtext dataset class
	print("Preprocessing train dataset...")
	train_dataset = Dataset(src_path=src_train,
					  		trg_path = trg_train,
					  		fields=fields,
					  		src_max_length=src_max_length,
					  		trg_max_length=trg_max_length
					  		)
	
	print("Preprocessing val dataset...")
	val_dataset = Dataset(src_path=src_val,
					  	  trg_path = trg_val,
					  	  fields=fields,
					  	  src_max_length=src_max_length,
					  	  trg_max_length=trg_max_length
					  	  )

	if share_vocab:
		vocab_save = os.path.join(save_path, save_name + '_vocab.txt')
		trg_field.build_vocab([train_dataset.src, train_dataset.trg], vocab_size=vocab_size, save_path=vocab_save)
		src_field.vocab = trg_field.vocab
	else:
		src_vocab_save = os.path.join(save_path, save_name + '_src_vocab.txt')
		trg_vocab_save = os.path.join(save_path, save_name + '_trg_vocab.txt')

		src_field.build_vocab(train_dataset.src, vocab_size=vocab_size, save_path=src_vocab_save)
		trg_field.build_vocab(train_dataset.trg, vocab_size=vocab_size, save_path=trg_vocab_save)


	train_save = os.path.join(save_path, save_name + '_train')
	val_save = os.path.join(save_path, save_name + '_val')

	print("Saving train dataset...")
	train_examples = vars(train_dataset)['examples']
	train_dataset = {'examples': train_examples}
	torch.save(train_dataset, train_save)
	print('Nb. of train examples: ', len(train_dataset['examples']))


	print("Saving val dataset...")
	val_examples = vars(val_dataset)['examples']
	val_dataset = {'examples': val_examples}
	torch.save(val_dataset, val_save)
	print('Nb. of val examples: ', len(val_dataset['examples']))


def load_data(train_file, val_file, vocab_size, src_max_length=None, trg_max_length=None, share_vocab=False, src_vocab=None, trg_vocab=None, split_level='word'):

	if split_level: torchtext.split_level=split_level

	len_filter = None
	if src_max_length and not trg_max_length:
		len_filter = lambda example: len(example.src) <= src_max_length
	if trg_max_length and not src_max_length:
		len_filter = lambda example: len(example.trg) <= trg_max_length
	if src_max_length and trg_max_length:
		len_filter = lambda example: len(example.src) <= src_max_length and len(example.trg) <= trg_max_length




	src_field, src_raw_field, trg_field, trg_raw_field = generate_fields()
	fields = [('src', src_field), ('src_raw', src_raw_field), ('trg', trg_field), ('trg_raw', trg_raw_field)]


	# Loading saved data
	train_dataset = torch.load(train_file)
	train_examples = train_dataset['examples']
	
	val_dataset = torch.load(val_file)
	val_examples = val_dataset['examples']


	train_dataset = torchtext.Dataset(fields=fields, examples=train_examples, filter_pred=len_filter)
	print('Nb. of train examples: ', len(train_dataset.examples))
	val_dataset = torchtext.Dataset(fields=fields, examples=val_examples, filter_pred=len_filter)
	print('Nb. of val examples: ', len(val_dataset.examples))
	

	# Building field vocabulary
	voc_dataset = torchtext.Dataset(fields=fields, examples=train_examples) # we build the vocab from train dataset without len_filter

	if share_vocab:
		trg_field.build_vocab([voc_dataset.src, voc_dataset.trg], vocab_size=vocab_size, load_path=trg_vocab)
		src_field.vocab = trg_field.vocab
	else:
		src_field.build_vocab(voc_dataset.src, vocab_size=vocab_size, load_path=src_vocab)
		trg_field.build_vocab(voc_dataset.trg, vocab_size=vocab_size, load_path=trg_vocab)


	source_lang = AttrDict()
	source_lang.src_field = src_field
	source_lang.word2index = src_field.vocab.word2index
	source_lang.index2word = src_field.vocab.index2word
	source_lang.n_words = len(source_lang.word2index)
	source_lang.PAD_id = source_lang.word2index[torchtext.PAD_token]
	source_lang.UNK_id = source_lang.word2index[torchtext.UNK_token]


	target_lang = AttrDict()
	target_lang.trg_field = trg_field
	target_lang.word2index = trg_field.vocab.word2index
	target_lang.index2word = trg_field.vocab.index2word
	target_lang.n_words = len(target_lang.word2index)
	target_lang.SOS_id = target_lang.word2index[torchtext.SOS_token]
	target_lang.EOS_id = target_lang.word2index[torchtext.EOS_token]
	target_lang.PAD_id = target_lang.word2index[torchtext.PAD_token]
	target_lang.UNK_id = target_lang.word2index[torchtext.UNK_token]


	print('Nb. of source vocab: ', source_lang.n_words)
	print('Nb. of target vocab: ', target_lang.n_words)


	train_dataset = Seq2SeqDataset(train_dataset, source_lang, target_lang)
	val_dataset = Seq2SeqDataset(val_dataset, source_lang, target_lang)


	# return train_dataset, val_dataset, source_lang, target_lang
	return train_dataset, val_dataset


def load_test_data(src_vocab, trg_vocab, share_vocab=False, split_level='word'):
	if split_level: torchtext.split_level=split_level

	src_field, src_raw_field, trg_field, trg_raw_field = generate_fields()
	fields = [('src', src_field), ('src_raw', src_raw_field)]


	if share_vocab:
		trg_field.build_vocab(load_path=trg_vocab)
		src_field.vocab = trg_field.vocab
	else:
		trg_field.build_vocab(load_path=trg_vocab)
		src_field.build_vocab(load_path=src_vocab)

	
	source_lang = AttrDict()
	source_lang.word2index = src_field.vocab.word2index
	source_lang.index2word = src_field.vocab.index2word
	source_lang.n_words = len(source_lang.word2index)
	source_lang.PAD_id = source_lang.word2index[torchtext.PAD_token]
	source_lang.UNK_id = source_lang.word2index[torchtext.UNK_token]


	target_lang = AttrDict()
	target_lang.word2index = trg_field.vocab.word2index
	target_lang.index2word = trg_field.vocab.index2word
	target_lang.n_words = len(target_lang.word2index)
	target_lang.SOS_id = target_lang.word2index[torchtext.SOS_token]
	target_lang.EOS_id = target_lang.word2index[torchtext.EOS_token]
	target_lang.PAD_id = target_lang.word2index[torchtext.PAD_token]
	target_lang.UNK_id = target_lang.word2index[torchtext.UNK_token]

	test_dataset = Seq2SeqDataset(None, source_lang, target_lang)
	
	return test_dataset, fields


class Seq2SeqDataset(object):

	def __init__(self, dataset, source_lang=None, target_lang=None):
		
		self.dataset = dataset

		if source_lang:
			self.source_lang = source_lang
		if target_lang:
			self.target_lang = target_lang


		
	def pg_scr2index(self, src_words):
		idx = []
		oovs = []
		UNK_id = self.source_lang.UNK_id

		for word in src_words:
			i = self.source_lang.word2index[word]
			if i == UNK_id:
				if word not in oovs:
					oovs.append(word)
				oov_num = oovs.index(word)
				idx.append(self.source_lang.n_words+oov_num)
			else:
				idx.append(i)

		return idx, oovs


	def pg_trg2index(self, trg_words, src_oovs):
		idx = []
		UNK_id = self.target_lang.UNK_id

		for word in trg_words:
			i = self.target_lang.word2index[word]
			if i == UNK_id:
				if word in src_oovs:
					vocab_index = self.target_lang.n_words + src_oovs.index(word)
					idx.append(vocab_index)
				else:
					idx.append(UNK_id)
			else:
				idx.append(i)

		# add EOS_id
		# idx.insert(0, self.target_lang.SOS_id)
		idx.append(self.target_lang.EOS_id)

		return idx


	def pg_index2words(self, idx, src_oovs):
		words = []
		for i in idx:
			try:
				word = self.target_lang.index2word[i] # might be [UNK]
			except: # word is OOV
				article_oov_idx = i - self.target_lang.n_words
				try:
					word = src_oovs[article_oov_idx]
				except: # i doesn't correspond to an article oov
					raise ValueError('Error')
			words.append(word)
		return words



	def pg_batch(self, src_seqs, src_raw_seqs, trg_seqs=None, trg_raw_seqs=None):

		def pad_seq(seq, max_length, PAD_id):
			seq += [PAD_id for i in range(max_length - len(seq))]
			return seq

		pg_src = []
		oovs = []
		pg_trg = []

		if src_seqs is not None and src_raw_seqs is not None and trg_seqs is not None and trg_raw_seqs is not None:

			for src_raw_seq, trg_raw_seq in zip(src_raw_seqs, trg_raw_seqs):

				src_raw_seq = torchtext.tokenizer(src_raw_seq)
				trg_raw_seq = torchtext.tokenizer(trg_raw_seq)

				src, src_oovs = self.pg_scr2index(src_raw_seq)
				trg = self.pg_trg2index(trg_raw_seq, src_oovs)

				pg_src.append(src)
				oovs.append(src_oovs)
				pg_trg.append(trg)

			max_pg_src = max([len(seq) for seq in pg_src])
			max_pg_trg = max([len(seq) for seq in pg_trg])

			pg_src = [pad_seq(seq, max_pg_src, self.source_lang.PAD_id) for seq in pg_src]
			pg_trg = [pad_seq(seq, max_pg_trg, self.source_lang.PAD_id) for seq in pg_trg]

			pg_src = torch.tensor(pg_src, dtype=torch.long, device=device)
			pg_trg = torch.tensor(pg_trg, dtype=torch.long, device=device)
			
			max_oovs = max([len(oov) for oov in oovs])

			assert (src_seqs.size() == pg_src.size()) and (trg_seqs.size() == pg_trg.size()), 'problem occured during pg_batch generation.'

			return pg_src, pg_trg, max_oovs, oovs
		
		else:
			for src_raw_seq in src_raw_seqs:
				src_raw_seq = torchtext.tokenizer(src_raw_seq)
				src, src_oovs = self.pg_scr2index(src_raw_seq)
				
				pg_src.append(src)
				oovs.append(src_oovs)

			max_pg_src = max([len(seq) for seq in pg_src])

			pg_src = [pad_seq(seq, max_pg_src, self.source_lang.PAD_id) for seq in pg_src]
			pg_src = torch.tensor(pg_src, dtype=torch.long, device=device)
			max_oovs = max([len(oov) for oov in oovs])

			assert (src_seqs.size() == pg_src.size()), 'problem occured during pg_batch generation. src_seqs: {}, pg_src: {}'.format(src_seqs.size(), pg_src.size())

			return pg_src, max_oovs, oovs




