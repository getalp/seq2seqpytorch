class AttrDict(dict):
	""" Access dictionary keys like attribute
		https://stackoverflow.com/questions/4984647/accessing-dict-keys-like-an-attribute
	"""
	def __init__(self, *av, **kav):
		dict.__init__(self, *av, **kav)
		self.__dict__ = self

	def __getstate__(self):
		return self.__dict__

	def __setstate__(self, state):
		self.__dict__.update(state)

	def __getattr__(self, key):
		if key.startswith('__') and key.endswith('__'):
			raise AttributeError
		return None