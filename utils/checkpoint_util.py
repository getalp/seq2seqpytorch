import os
import glob

import torch



def load_checkpoint(opts, checkpoint_file=None, verbose=True):
	
	if not 'experiment_dir' in opts:
		opts.experiment_dir = './experiments/'
	experiment_dir = os.path.join(opts.experiment_dir, opts.experiment_name)
	checkpoint_dir = os.path.join(experiment_dir, 'checkpoints/')

	if checkpoint_file is None:
		#Load the latest checkpoint (also best wrt to train loss)
		list_of_checkpoints = glob.glob('{}/*'.format(checkpoint_dir))
		checkpoint_file = max(list_of_checkpoints, key=os.path.getctime)

	checkpoint = torch.load(checkpoint_file, map_location=lambda storage, loc: storage)

	if verbose:
		print('='*100)
		print('Checkpoint {} was loaded successfully...'.format(checkpoint_file))
		print('='*100)

	return checkpoint


def save_checkpoint(opts, model, optimizer, lr_scheduler, last_loss, global_step, train_loss_history, start_time=None, verbose=True):
	checkpoint = {
		'opts': opts,
		'global_step': global_step,
		'train_loss_history': train_loss_history,
		'model_state_dict': model.state_dict(),
		'optimizer_state_dict': optimizer.state_dict(),
		'lr_scheduler_state_dict': lr_scheduler.state_dict(),
		'start_time': start_time,
	}

	if not 'experiment_dir' in opts:
		opts.experiment_dir = './experiments/'
	experiment_dir = os.path.join(opts.experiment_dir, opts.experiment_name)
	checkpoint_dir = os.path.join(experiment_dir, 'checkpoints/')
	if not os.path.exists(checkpoint_dir): os.makedirs(checkpoint_dir)



	checkpoint_file = '%s/%s_step_%d_loss_%.2f.pt' % (checkpoint_dir, opts.experiment_name, global_step, last_loss)

	torch.save(checkpoint, checkpoint_file)
	
	if verbose:
		print('='*100)
		print('Checkpoint {}, was saved successfully...'.format(checkpoint_file))
		print('='*100)

	return checkpoint_file
