import copy

import numpy as np
import torch
import torch.nn as nn

def tile(x, count, dim=0):
	"""
    Tiles x on dimension dim count times.
	"""
	if isinstance(x, tuple):
		h, c = x
		return tile(h, count, dim=dim), tile(c, count, dim=dim)

	perm = list(range(len(x.size())))
	if dim != 0:
		perm[0], perm[dim] = perm[dim], perm[0]
		x = x.permute(perm).contiguous()
	out_size = list(x.size())
	out_size[0] *= count
	batch = x.size(0)
	x = x.view(batch, -1) \
		.transpose(0, 1) \
		.repeat(count, 1) \
		.transpose(0, 1) \
		.contiguous() \
		.view(*out_size)
	if dim != 0:
		x = x.permute(perm).contiguous()
	return x



def subsequent_mask(size):
	"Mask out subsequent positions."
	attn_shape = (1, size, size)
	subsequent_mask = np.triu(np.ones(attn_shape), k=1).astype('uint8')
	return torch.from_numpy(subsequent_mask) == 0
	
def make_std_mask(trg, pad_id):
	"Create a mask to hide padding and future words."
	trg_mask = (trg != pad_id).unsqueeze(-2)
	trg_mask = trg_mask & subsequent_mask(trg.size(-1)).type_as(trg_mask.data)
	return trg_mask	



def clones(module, N):
	'''Produce N identical layers.'''
	return nn.ModuleList([copy.deepcopy(module) for _ in range(N)])
